const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

mix.js('resources/js/autores.js', 'public/js');
mix.js('resources/js/campus.js', 'public/js');
mix.js('resources/js/categorias.js', 'public/js');
mix.js('resources/js/editoriales.js', 'public/js');
mix.js('resources/js/identificaciones.js', 'public/js');
mix.js('resources/js/estatus-ejemplares.js', 'public/js');
mix.js('resources/js/facultades.js', 'public/js');
mix.js('resources/js/tipos-lectores.js', 'public/js');
mix.js('resources/js/sucursales.js', 'public/js');
mix.js('resources/js/libros.js', 'public/js')
    .sass('resources/sass/libros.scss', 'public/css');

mix.js('resources/js/detalle-libro.js', 'public/js');
mix.js('resources/js/lectores.js', 'public/js');
mix.js('resources/js/usuarios.js', 'public/js');
mix.js('resources/js/home.js', 'public/js');
mix.js('resources/js/prestamos.js', 'public/js');
mix.js('resources/js/reportes.js', 'public/js');
mix.js('resources/js/bitacora.js', 'public/js');