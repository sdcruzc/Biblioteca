<?php

namespace Database\Seeders;
use App\Models\Sucursales;
use Illuminate\Database\Seeder;

class sucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sucursales::create(["sucursal"=>"Puebla norte","creado_por"=>1,"modificado_por"=>1]);
        Sucursales::create(["sucursal"=>"Puebla sur","creado_por"=>1,"modificado_por"=>1]);
        Sucursales::create(["sucursal"=>"Mexico periplaza","creado_por"=>1,"modificado_por"=>1]);
        Sucursales::create(["sucursal"=>"Mexico este","creado_por"=>1,"modificado_por"=>1]);
        Sucursales::create(["sucursal"=>"Mexico sur","creado_por"=>1,"modificado_por"=>1]);
        Sucursales::create(["sucursal"=>"Monterrey plaza","creado_por"=>1,"modificado_por"=>1]);
        Sucursales::create(["sucursal"=>"Monterrey suroeste","creado_por"=>1,"modificado_por"=>1]);
    }
}
