<?php

namespace Database\Seeders;
use App\Models\Autores;
use Illuminate\Database\Seeder;

class autoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Autores::create(["nombre"=>"Andrés","apellidos"=>"Oppenheimer","creado_por"=>1,"modificado_por"=>1]);
        Autores::create(["nombre"=>"Carlos","apellidos"=>"Cuahutémoc Sánchez","creado_por"=>1,"modificado_por"=>1]);
        Autores::create(["nombre"=>"Gene","apellidos"=>"Kim","creado_por"=>1,"modificado_por"=>1]);
        Autores::create(["nombre"=>"Kevin","apellidos"=>"Beher","creado_por"=>1,"modificado_por"=>1]);
        Autores::create(["nombre"=>"Francisco Javier","apellidos"=>"Ceballos Sierra","creado_por"=>1,"modificado_por"=>1]);
        Autores::create(["nombre"=>"Francisco","apellidos"=>"Blasco","creado_por"=>1,"modificado_por"=>1]);
        Autores::create(["nombre"=>"Paul","apellidos"=>"Deitel","creado_por"=>1,"modificado_por"=>1]);
    }
}
