<?php

namespace Database\Seeders;
use App\Models\Facultades;
use Illuminate\Database\Seeder;

class facultadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Facultades::create(["facultad"=>"Ingeniería","creado_por"=>1,"modificado_por"=>1]);
        Facultades::create(["facultad"=>"Medicina","creado_por"=>1,"modificado_por"=>1]);
        Facultades::create(["facultad"=>"Humanidades","creado_por"=>1,"modificado_por"=>1]);
        Facultades::create(["facultad"=>"Matemáticas","creado_por"=>1,"modificado_por"=>1]);
        Facultades::create(["facultad"=>"Ciencias","creado_por"=>1,"modificado_por"=>1]);
        Facultades::create(["facultad"=>"Leyes","creado_por"=>1,"modificado_por"=>1]);
    }
}
