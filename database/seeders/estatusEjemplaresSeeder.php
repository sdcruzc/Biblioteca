<?php

namespace Database\Seeders;
use App\Models\Estatus_ejemplares;
use Illuminate\Database\Seeder;

class estatusEjemplaresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estatus_ejemplares::create(["estatus"=>"Prestado","creado_por"=>1,"modificado_por"=>1]);
        Estatus_ejemplares::create(["estatus"=>"Baja","creado_por"=>1,"modificado_por"=>1]);
        Estatus_ejemplares::create(["estatus"=>"Disponible","creado_por"=>1,"modificado_por"=>1]);
        Estatus_ejemplares::create(["estatus"=>"Solo lectura","creado_por"=>1,"modificado_por"=>1]);
    }
}
