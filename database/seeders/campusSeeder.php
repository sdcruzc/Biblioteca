<?php

namespace Database\Seeders;
use App\Models\Campus;
use Illuminate\Database\Seeder;

class campusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campus::create(["campus"=>"Puebla","creado_por"=>1,"modificado_por"=>1]);
        Campus::create(["campus"=>"Monterrey","creado_por"=>1,"modificado_por"=>1]);
        Campus::create(["campus"=>"Tehuacán","creado_por"=>1,"modificado_por"=>1]);
        Campus::create(["campus"=>"Veracruz","creado_por"=>1,"modificado_por"=>1]);
        Campus::create(["campus"=>"Oaxaca","creado_por"=>1,"modificado_por"=>1]);
    }
}
