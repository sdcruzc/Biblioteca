<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role:: create(["name" => "Super Admin"]);
        Role:: create(["name" => "Admin Sucursales"]);
        Role:: create(["name" => "Bibliotecario"]);
        Role:: create(["name" => "Asistente"]);
        $user= User::find(1);
        $user->assignRole('Super Admin');
    }
}
