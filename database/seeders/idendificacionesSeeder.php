<?php

namespace Database\Seeders;
use App\Models\Tipo_identificacion;
use Illuminate\Database\Seeder;

class idendificacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tipo_identificacion::create(["identificacion"=>"INE","creado_por"=>1,"modificado_por"=>1]);
        Tipo_identificacion::create(["identificacion"=>"Credencial","creado_por"=>1,"modificado_por"=>1]);
        Tipo_identificacion::create(["identificacion"=>"Licencia","creado_por"=>1,"modificado_por"=>1]);
        Tipo_identificacion::create(["identificacion"=>"Cédula","creado_por"=>1,"modificado_por"=>1]);
    }
}
