<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Categorias;

class categoriasSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $listado= [
            "Administración","Agricultura","Arquitectura",
            "Arte","Automotriz","Biología","Ciencia ficción",
            "Ciencias Básicas","Ciencias de la salud","Ciencias políticas",
            "Derecho","Diseño de modas","Diseño gráfico",
            "Economía","Educación","Estadística","Ética",
            "Filosofía","Física","Fotografía","Historia",
            "Idiomas","Informática","Ingeniería","Literatura",
            "Lógica","Matemáticas","Novelas","Psicología","Química"
        ];
        
        foreach($listado as $val){
            $categoria = new Categorias();
            $categoria->categoria = $val;
            $categoria->creado_por = 1;
            $categoria->modificado_por = 1;
            $categoria->save();
        }
    }
}
