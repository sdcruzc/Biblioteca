<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            categoriasSeeder::class,
            bitacoraSeeder::class,
            autoresSeeder::class,
            sucursalesSeeder::class,
            campusSeeder::class,
            editorialesSeeder::class,
            facultadesSeeder::class,
            estatusEjemplaresSeeder::class,
            idendificacionesSeeder::class,
            tipoLectoresSeeder::class
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
