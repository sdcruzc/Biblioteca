<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Bitacora;

class bitacoraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bitacora = new Bitacora();
        $bitacora->id_usuario = 1;
        $bitacora->accion = "Creación de base de datos y configuración inicial";
        $bitacora->save();
    }
}
