<?php

namespace Database\Seeders;
use App\Models\Tipo_lector;
use Illuminate\Database\Seeder;

class tipoLectoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tipo_lector::create(["tipo_lector"=>"Administrativo","creado_por"=>1,"modificado_por"=>1]);
        Tipo_lector::create(["tipo_lector"=>"Profesor","creado_por"=>1,"modificado_por"=>1]);
        Tipo_lector::create(["tipo_lector"=>"Alumno","creado_por"=>1,"modificado_por"=>1]);
        Tipo_lector::create(["tipo_lector"=>"Ex alumno","creado_por"=>1,"modificado_por"=>1]);
        Tipo_lector::create(["tipo_lector"=>"Externo","creado_por"=>1,"modificado_por"=>1]);
    }
}
