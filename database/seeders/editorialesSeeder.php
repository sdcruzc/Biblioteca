<?php

namespace Database\Seeders;
use App\Models\Editoriales;
use Illuminate\Database\Seeder;

class editorialesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Editoriales::create(["editorial"=>"Diamante","creado_por"=>1,"modificado_por"=>1]);
        Editoriales::create(["editorial"=>"Grijalbo","creado_por"=>1,"modificado_por"=>1]);
        Editoriales::create(["editorial"=>"Penguin Random House","creado_por"=>1,"modificado_por"=>1]);
        Editoriales::create(["editorial"=>"Alfaomega","creado_por"=>1,"modificado_por"=>1]);
        Editoriales::create(["editorial"=>"Ra-Ma","creado_por"=>1,"modificado_por"=>1]);
        Editoriales::create(["editorial"=>"It Revolution Press","creado_por"=>1,"modificado_por"=>1]);
    }
}
