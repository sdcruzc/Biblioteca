<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLectores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectores', function (Blueprint $table) {
            $table->id();
            $table->string('matricula',30)->nullable();
            $table->string('nombre',80);
            $table->string('apellidos',100);
            $table->date('fecha_nacimiento')->nullable();
            $table->string('curp',30)->nullable();
            $table->string('celular',10);
            $table->string('telefono',10)->nullable();
            $table->string('correo',80);
            $table->string('direccion',200)->nullable();
            $table->foreignId('id_campus')->constrained('campus')->onUpdate('cascade')->onDelete('restrict')->nullable();
            $table->foreignId('id_facultad')->constrained('facultades')->onUpdate('cascade')->onDelete('restrict')->nullable();
            $table->foreignId('id_tipo_lector')->constrained('tipo_lector')->onUpdate('cascade')->onDelete('restrict')->nullable();
            $table->foreignId('creado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('modificado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectores');
    }
}
