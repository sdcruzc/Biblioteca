<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosSucursales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_sucursales', function (Blueprint $table) {
            $table->foreignId('id_usuario')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_sucursal')->constrained('sucursales')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('creado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('modificado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
            $table->primary(['id_usuario', 'id_sucursal']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_sucursales');
    }
}
