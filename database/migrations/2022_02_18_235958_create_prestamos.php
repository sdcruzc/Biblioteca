<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrestamos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_lector')->constrained('lectores')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_ejemplar')->constrained('ejemplares')->onUpdate('cascade')->onDelete('restrict');
            $table->datetime('fecha_prestamo');
            $table->datetime('fecha_devolucion');
            $table->datetime('fecha_devolucion_real')->nullable();
            $table->foreignId('id_tipo_identificacion')->constrained('tipo_identificacion')->onUpdate('cascade')->onDelete('restrict')->nullable();
            $table->foreignId('creado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('modificado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamos');
    }
}
