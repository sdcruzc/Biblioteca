<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->id();
            $table->string("isbn",50);
            $table->string("titulo",100);
            $table->string("subtitulo",100)->nullable();
            $table->string("descripcion",250)->nullable();
            $table->integer("paginas")->nullable();
            $table->string('publicacion',5)->nullable();
            $table->foreignId('id_editorial')->constrained('editoriales')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_categoria')->constrained('categorias')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('creado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('modificado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
    }
}
