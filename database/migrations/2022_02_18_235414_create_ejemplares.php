<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEjemplares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ejemplares', function (Blueprint $table) {
            $table->id();
            $table->string('codigo',30);
            $table->string('pasillo',15)->nullable();
            $table->string('estante',15)->nullable();
            $table->foreignId('id_sucursal')->constrained('editoriales')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_libro')->constrained('categorias')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_estatus')->constrained('categorias')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('creado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('modificado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ejemplares');
    }
}
