<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoLector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_lector', function (Blueprint $table) {
            $table->id();
            $table->string("tipo_lector",80);
            $table->foreignId('creado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('modificado_por')->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_lector');
    }
}
