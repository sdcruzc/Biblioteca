Sistema para bibliotecas universitarias, totalmente configurable y adaptable a dispositivos móviles con interfaz amigable
<p>Lenguaje: <b>PHP 7.4</b></p>
<p>Framework <b>Laravel 8</b></p>
<p>Utilizando <b>Bootstrap 5</b>, <b>jQuery</b>, <b>DataTables</b>, <b>animate</b>, entre otras</p>

<b>Módulos:</b>
<li>
    <ul>- Gestión de roles</ul>
    <ul>- Sucursales</ul>
    <ul>- Campus</ul>
    <ul>- Facultades</ul>
    <ul>- Editoriales</ul>
    <ul>- Categorías</ul>
    <ul>- Autores</ul>
    <ul>- Libros</ul>
    <ul>- Ejemplares</ul>
    <ul>- Lectores</ul>
    <ul>- Tipos de lectores</ul>
    <ul>- Préstamos</ul>
    <ul>- Tipos de identificación</ul>
    <ul>- Reportes exportables a PDF y Excel</ul>
    <ul>- Bitácora de movimientos</ul>
</li>

<p>Instalación:</p>
<li>
    <ol>Ejecutar las migraciones: <b>php artisan migrate --seed</b></ol>
    <ol>Instalar las librerías: <b>npm install</b></ol>
    <ol>Compilar JS y CSS: <b>npm run production</b></ol>
</li>