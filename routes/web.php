<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group( ["middleware" => ['auth']], function(){
    Route::post('change-password', [App\Http\Controllers\UserController::class, 'updatePassword']);
});

Route::group( ["middleware" => ['auth',"role:Super Admin|Admin Sucursales"]], function(){
    Route::get('usuarios', [App\Http\Controllers\UserController::class, 'index']);
    Route::post('save-user', [App\Http\Controllers\UserController::class, 'store']);
    Route::post('update-user', [App\Http\Controllers\UserController::class, 'update']);
    Route::post('delete-user', [App\Http\Controllers\UserController::class, 'delete']);
    Route::get('bitacora', [App\Http\Controllers\BitacoraController::class, 'index']);
    Route::post('show-bitacora', [App\Http\Controllers\BitacoraController::class, 'show']);

    Route::get('campus', [App\Http\Controllers\CampusController::class, 'index']);
    Route::post('save-campus', [App\Http\Controllers\CampusController::class, 'store']);
    Route::post('update-campus', [App\Http\Controllers\CampusController::class, 'update']);
    Route::post('delete-campus', [App\Http\Controllers\CampusController::class, 'delete']);

    Route::get('estatus-ejemplares', [App\Http\Controllers\EstatusEjemplaresController::class, 'index']);
    Route::post('save-estatus-ejemplares', [App\Http\Controllers\EstatusEjemplaresController::class, 'store']);
    Route::post('update-estatus-ejemplares', [App\Http\Controllers\EstatusEjemplaresController::class, 'update']);
    Route::post('delete-estatus-ejemplares', [App\Http\Controllers\EstatusEjemplaresController::class, 'delete']);

    Route::get('facultades', [App\Http\Controllers\FacultadesController::class, 'index']);
    Route::post('save-facultad', [App\Http\Controllers\FacultadesController::class, 'store']);
    Route::post('update-facultad', [App\Http\Controllers\FacultadesController::class, 'update']);
    Route::post('delete-facultad', [App\Http\Controllers\FacultadesController::class, 'delete']);

    Route::get('identificaciones', [App\Http\Controllers\TipoIdentificacionController::class, 'index']);
    Route::post('save-identificacion', [App\Http\Controllers\TipoIdentificacionController::class, 'store']);
    Route::post('update-identificacion', [App\Http\Controllers\TipoIdentificacionController::class, 'update']);
    Route::post('delete-identificacion', [App\Http\Controllers\TipoIdentificacionController::class, 'delete']);

    Route::get('tipos-lectores', [App\Http\Controllers\TipoLectorController::class, 'index']);
    Route::post('save-tipos-lectores', [App\Http\Controllers\TipoLectorController::class, 'store']);
    Route::post('update-tipos-lectores', [App\Http\Controllers\TipoLectorController::class, 'update']);
    Route::post('delete-tipos-lectores', [App\Http\Controllers\TipoLectorController::class, 'delete']);

    Route::get('sucursales', [App\Http\Controllers\SucursalesController::class, 'index']);
    Route::post('save-sucursal', [App\Http\Controllers\SucursalesController::class, 'store']);
    Route::post('update-sucursal', [App\Http\Controllers\SucursalesController::class, 'update']);
    Route::post('delete-sucursal', [App\Http\Controllers\SucursalesController::class, 'delete']);

});
Route::group(['middleware' => ['auth','role:Super Admin|Admin Sucursales|Bibliotecario']], function () {
    Route::get('autores', [App\Http\Controllers\AutoresController::class, 'index']);
    Route::post('save-autor', [App\Http\Controllers\AutoresController::class, 'store']);
    Route::post('update-autor', [App\Http\Controllers\AutoresController::class, 'update']);
    Route::post('delete-autor', [App\Http\Controllers\AutoresController::class, 'delete']);

    Route::get('categorias', [App\Http\Controllers\CategoriasController::class, 'index']);
    Route::post('save-categoria', [App\Http\Controllers\CategoriasController::class, 'store']);
    Route::post('update-categoria', [App\Http\Controllers\CategoriasController::class, 'update']);
    Route::post('delete-categoria', [App\Http\Controllers\CategoriasController::class, 'delete']);
    
    Route::get('editoriales', [App\Http\Controllers\EditorialesController::class, 'index']);
    Route::post('save-editorial', [App\Http\Controllers\EditorialesController::class, 'store']);
    Route::post('update-editorial', [App\Http\Controllers\EditorialesController::class, 'update']);
    Route::post('delete-editorial', [App\Http\Controllers\EditorialesController::class, 'delete']);
    
    Route::get('crear-libros', [App\Http\Controllers\LibrosController::class, 'create']);
    Route::post('save-libro', [App\Http\Controllers\LibrosController::class, 'store']);
    Route::resource('libros', App\Http\Controllers\LibrosController::class);

    Route::post('save-ejemplar', [App\Http\Controllers\EjemplaresController::class, 'store']);
    Route::post('update-ejemplar', [App\Http\Controllers\EjemplaresController::class, 'update']);
    Route::post('delete-ejemplar', [App\Http\Controllers\EjemplaresController::class, 'delete']);
    Route::post('show-ejemplar', [App\Http\Controllers\EjemplaresController::class, 'show']);

    Route::get('lectores', [App\Http\Controllers\LectoresController::class, 'index']);
    Route::post('save-lector', [App\Http\Controllers\LectoresController::class, 'store']);
    Route::post('update-lector', [App\Http\Controllers\LectoresController::class, 'update']);
    Route::post('delete-lector', [App\Http\Controllers\LectoresController::class, 'delete']);
    Route::post('show-lector', [App\Http\Controllers\LectoresController::class, 'show']);

    Route::get('prestamos', [App\Http\Controllers\PrestamosController::class, 'index']);
    Route::post('save-prestamo', [App\Http\Controllers\PrestamosController::class, 'store']);
    Route::post('save-devolucion', [App\Http\Controllers\PrestamosController::class, 'devolver']);

    Route::get('reportes', [App\Http\Controllers\ReportesController::class, 'index']);
    Route::post('show-reporte', [App\Http\Controllers\ReportesController::class, 'show']);


});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
