<?php

namespace App\Http\Controllers;
use App\Models\Categorias;
use App\Models\Libros;
use Illuminate\Http\Request;

class CategoriasController extends Controller{
    function index(){
        $top = Categorias::selectRaw("categoria, COUNT(id_categoria) total")
                ->join("libros","libros.id_categoria","=","categorias.id")
                ->orderByDesc('total')
                ->groupBy('categoria')
                ->limit(10)
                ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de categorías");
        $categorias = Categorias::orderBy('categoria')->get();
        return view('categorias',compact('categorias','top'));
    }

    function store(Request $request){
        $nombre= trim($request->categoria);
        if(Categorias::where("categoria", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear una nueva categoría: ".$nombre." pero no se pudo debido a que ya existe otra categoría con el mismo nombre");
            return redirect("/categorias")->with("error", "Ya existe una categoria con el nombre ".$nombre);
        }
        else{
            $categoria = new Categorias();
            $categoria->categoria= $nombre;
            $categoria->creado_por = auth()->user()->id;
            $categoria->modificado_por = auth()->user()->id;
            $categoria->save();
            BitacoraController::saveBitacora("Crea una nueva categoría: ".$nombre);
            return redirect("/categorias")->with("success", "Categoria ".$nombre." creada");
        }
    }

    function update(Request $request){
        $id= $request->idCategoria;
        $nombre= trim($request->categoria);
        $categoria= Categorias::find($id);
        if($categoria){
            if(Categorias::where("categoria", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar la categoría: ".$nombre." pero no se pudo debido a que ya existe otra categoría con el mismo nombre");
                return redirect("/categorias")->with("error", "No se puede modificar debido a que ya existe una categoria con el nombre ".$nombre);
            }
            else{
                $categoria->categoria=$nombre;
                $categoria->modificado_por = auth()->user()->id;
                $categoria->save();
                BitacoraController::saveBitacora("Modifica la categoría: ".$nombre);
                return redirect("/categorias")->with("success", "Categoria modificada ".$nombre);
            }
        }
        else{
            return redirect("/categorias")->with("error", "No existe la categoría ");
        }
        
    }

    function delete(Request $request){
        $id= $request->idCat;
        $categoria= Categorias::find($id);
        if($categoria){
            $nombre= $categoria->categoria;
            if(Libros::where("id_categoria",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar la categoría: ".$nombre." pero no se pudo debido a que ya se usa en algún libro");
                return redirect("/categorias")->with("error", "No se puede eliminar la categoría debido a que ya existen libros con esa categoría");
            }
            else{
                $categoria->destroy($id);
                BitacoraController::saveBitacora("Elimina la categoría: ".$nombre);
                return redirect("/categorias")->with("success", "Categoria eliminada ");
            }
        }
        else{
            return redirect("/categorias")->with("error", "No existe la categoría ");
        }

    }
}
