<?php

namespace App\Http\Controllers;
use App\Models\Libros;
use App\Models\Ejemplares;
use App\Models\Autores_libros;
use App\Models\Editoriales;
use App\Models\Categorias;
use App\Models\Autores;
use App\Models\Sucursales;
use App\Models\Estatus_ejemplares;
use Illuminate\Http\Request;

class LibrosController extends Controller{
    function index(){
        BitacoraController::saveBitacora("Ingresa al módulo de libros");
        $libros = $this->getLibros();
        return view('libros',compact('libros'));
    }

    function create(){
        BitacoraController::saveBitacora("Ingresa al módulo para registrar libros");
        $editoriales = Editoriales::orderBy('editorial')->get();
        $categorias = Categorias::orderBy('categoria')->get();
        $autores = Autores::orderBy('apellidos')->get();
        return view('crear_libros',compact('categorias','editoriales','autores'));
    }

    function store(Request $request){
        $isbn= trim($request->isbn);
        $titulo= trim($request->titulo);
        $subtitulo= trim($request->subtitulo);
        $descripcion= trim($request->descripcion);
        $paginas= $request->paginas;
        $publicacion= trim($request->publicacion);
        $editorial= trim($request->editorial);
        $categoria= trim($request->categoria);
        $autores= array_unique($request->autores);
        //Guardar libros
        $libro= new Libros();
        $libro->isbn = $isbn;
        $libro->titulo = $titulo;
        $libro->subtitulo = $subtitulo;
        $libro->descripcion = $descripcion;
        $libro->paginas = $paginas;
        $libro->publicacion = $publicacion;
        $libro->id_editorial = $editorial;
        $libro->id_categoria = $categoria;
        $libro->creado_por = auth()->user()->id;
        $libro->modificado_por = auth()->user()->id;
        $libro->save();
        //Guardar autores libros
        $idlibro = $libro->id;
        foreach($autores as $val){
            $autores_libros= new Autores_libros();
            $autores_libros->id_libro= $idlibro;
            $autores_libros->id_autor= $val;
            $autores_libros->creado_por = auth()->user()->id;
            $autores_libros->modificado_por = auth()->user()->id;
            $autores_libros->save();
        }
        return redirect("/libros")->with("success", "Libro ".$titulo." creado");
        
    }

    function getLibros($id=null){
        $where= (isset($id))?[["libros.id",$id]]:[];
        $libros = Libros::select("libros.id","isbn","titulo","subtitulo","descripcion","paginas","publicacion","editorial","categoria",\DB::raw("GROUP_CONCAT(CONCAT(autores.nombre,' ',autores.apellidos)) as autores"),\DB::raw("categoria,GROUP_CONCAT(autores_libros.id_autor) as idautores"))
        ->join("editoriales","libros.id_editorial","=","editoriales.id")
        ->join("categorias","libros.id_categoria","=","categorias.id")
        ->leftJoin('autores_libros', function($join) {
            $join->on(\DB::raw('FIND_IN_SET(libros.id , autores_libros.id_libro)'), '>', \DB::raw("0"));
        })
        ->leftJoin('autores', function($join) {
            $join->on(\DB::raw('FIND_IN_SET(autores_libros.id_autor , autores.id)'), '>', \DB::raw("0"));
        })
        ->where($where)
        ->orderBy('titulo')
        ->groupBy('libros.id')
        ->get();
        return $libros;
    }

    function getEjemplares($idLibro,$idSucursal =null){
        $where= (isset($idSucursal))?[["id_sucursal",$idSucursal],["id_libro",$idLibro]]:[["id_libro",$idLibro]];
        $ejemplares = Ejemplares::select("ejemplares.id","codigo","pasillo","estante","id_sucursal","id_libro","id_estatus","sucursal","estatus")
        ->join("sucursales","sucursales.id","=","ejemplares.id_sucursal")
        ->join("estatus_ejemplar","estatus_ejemplar.id","=","ejemplares.id_estatus")
        ->where($where)
        ->orderBy('id_sucursal')
        ->orderBy('id_estatus')
        ->get();
        return $ejemplares;
    }

    public function show($id){
        $libro = $this->getLibros($id);
        $ejemplares = $this->getEjemplares($id);
        $sucursales= Sucursales::all();
        $estatus= Estatus_ejemplares::where("id",">",2)->get();
        $home = new HomeController();
        $suc= $home->getSucursalUsuario(auth()->user()->id);
        $sucursal= (count($suc)>0) ? $suc[0]->id_sucursal : "0";
        return view('detalleLibros',compact('libro','ejemplares','sucursales','estatus','sucursal'));
    }
    
    function getTopPrestamos($sucursal){
        $where_array=($sucursal == 0) ? []: [["id_sucursal",$sucursal]];
        $consulta= Libros::select(\DB::raw("COUNT(libros.id) as total"),"isbn","titulo")
        ->join("ejemplares","ejemplares.id_libro","=","libros.id")
        ->join("prestamos","prestamos.id_ejemplar","=","ejemplares.id")
        ->where($where_array)
        ->groupBy("libros.id","titulo")
        ->limit(10)
        ->get();
        return $consulta;
    }

    function sinPrestar(){
        

    }
}
