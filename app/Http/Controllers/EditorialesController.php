<?php

namespace App\Http\Controllers;
use App\Models\Editoriales;
use App\Models\Libros;
use Illuminate\Http\Request;

class EditorialesController extends Controller{
    function index(){
        $top = Editoriales::selectRaw("editorial, COUNT(id_editorial) total")
                ->join("libros","libros.id_editorial","=","editoriales.id")
                ->orderByDesc('total')
                ->groupBy('editorial')
                ->limit(10)
                ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de editoriales");
        $editoriales = Editoriales::orderBy('editorial')->get();
        return view('editoriales',compact('editoriales','top'));
    }

    function store(Request $request){
        $nombre= trim($request->editorial);
        if(Editoriales::where("editorial", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear una nueva editorial: ".$nombre." pero no se pudo debido a que ya existe otra editorial con el mismo nombre");
            return redirect("/editoriales")->with("error", "Ya existe una editorial con el nombre ".$nombre);
        }
        else{
            $editorial = new Editoriales();
            $editorial->editorial= $nombre;
            $editorial->creado_por = auth()->user()->id;
            $editorial->modificado_por = auth()->user()->id;
            $editorial->save();
            BitacoraController::saveBitacora("Crea una nueva editorial: ".$nombre);
            return redirect("/editoriales")->with("success", "Editorial ".$nombre." creada");
        }
    }

    function update(Request $request){
        $id= $request->ideditorial;
        $nombre= trim($request->editorial);
        $editorial= Editoriales::find($id);
        if($editorial){
            if(Editoriales::where("editorial", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar la editorial: ".$nombre." pero no se pudo debido a que ya existe otra editorial con el mismo nombre");
                return redirect("/editoriales")->with("error", "No se puede modificar debido a que ya existe una editorial con el nombre ".$nombre);
            }
            else{
                $editorial->editorial=$nombre;
                $editorial->modificado_por = auth()->user()->id;
                $editorial->save();
                BitacoraController::saveBitacora("Modifica la editorial: ".$nombre);
                return redirect("/editoriales")->with("success", "Editorial modificada ".$nombre);
            }
        }
        else{
            return redirect("/editoriales")->with("error", "No existe la editorial ");
        }
        
    }
    function delete(Request $request){
        $id= $request->idEdi;
        $editorial= Editoriales::find($id);
        if($editorial){
            $nombre= $editorial->editorial;
            if(Libros::where("id_editorial",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar la editorial: ".$nombre." pero no se pudo debido a que ya se usa en algún libro");
                return redirect("/editoriales")->with("error", "No se puede eliminar la editorial debido a que ya existen libros con esa editorial");
            }
            else{
                $editorial->destroy($id);
                BitacoraController::saveBitacora("Elimina la editorial: ".$nombre);
                return redirect("/editoriales")->with("success", "Editorial eliminada ");
            }
        }
        else{
            return redirect("/editoriales")->with("error", "No existe la editorial ");
        }
    }
}