<?php

namespace App\Http\Controllers;
use App\Models\Bitacora;
use App\Models\User;
use Illuminate\Http\Request;

class BitacoraController extends Controller{
    
    public static function saveBitacora($accion){
        $bitacora = new Bitacora();
        $bitacora->id_usuario = auth()->user()->id;
        $bitacora->accion = $accion;
        $bitacora->save();
    }
    public function index(){
        $users = User::select("id","name")->where("id",">","1")->get();
        return view('bitacora',compact('users'));
    }
    public function show(Request $resquest){
        $user= $resquest->usuario;
        $inicio= $resquest->inicio;
        $fin= $resquest->fin;
        $where_array=[];
        if($user !=null){
            array_push($where_array,["id_usuario","=","$user"]);
        }
        if($inicio != null && $fin !=null){
            array_push($where_array,["fecha",">=","$inicio 00:00:00"]);
            array_push($where_array,["fecha","<=","$fin 23:59:59"]);
        }
        $registros = Bitacora::select("id_usuario","accion",\DB::raw("date_format(fecha, '%d/%m/%Y') as fecha2"),\DB::raw("date_format(fecha, '%H:%i:%s') as hora"),"fecha","name")
        ->join("users","users.id","=","bitacoras.id_usuario")
        ->where($where_array)
        ->orderBy('fecha')
        ->get();
        return $registros;
    }
}
