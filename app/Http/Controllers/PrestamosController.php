<?php

namespace App\Http\Controllers;
use App\Models\Ejemplares;
use App\Models\Lectores;
use App\Models\Tipo_identificacion;
use App\Models\Prestamos;
use App\Models\Libros;
use Illuminate\Http\Request;

class PrestamosController extends Controller{
    function index(){
        $usuario= auth()->user()->id;
        $home = new HomeController();
        $info_suc = $home->getSucursalUsuario($usuario);
        $sucursal= (count($info_suc)>0)? $info_suc[0]->id_sucursal:0;
        $identificaciones = Tipo_identificacion::orderBy('identificacion')->get();
        $hoy= date('Y-m-d');
        $prestamos =$this->show(null,null,null,$sucursal);
        BitacoraController::saveBitacora("Ingresa al módulo de préstamos");
        return view('prestamos',compact('identificaciones','sucursal','prestamos'));
    }

    function store(Request $request){
        $idLector= $request->idLector;
        $idEjemplar= $request->idEjemplar; 
        $fecha_devolucion = $request->fecha;
        $identificacion= $request->identificacion;
        $mensaje="";
        $lector= Lectores::find($idLector);
        $ejemplar= Ejemplares::find($idEjemplar);
        $tipo_identificacion= Tipo_identificacion::find($identificacion);
        if(!$lector){
            $mensaje="No existe el lector";
            return redirect("/prestamos")->with("error", "No se pudo realizar el préstamo. ".$mensaje);
        }
        else if(!$ejemplar){
            $mensaje="No existe el ejemplar";
            return redirect("/prestamos")->with("error", "No se pudo realizar el préstamo. ".$mensaje);
        }
        else if($ejemplar->id_estatus != 3){
            $mensaje="El ejemplar ".$ejemplar->codigo." no está disponible para préstamo";
            return redirect("/prestamos")->with("error", "No se pudo realizar el préstamo. ".$mensaje);
        }
        else if(!$tipo_identificacion){
            $mensaje="No existe el tipo de identificación";
            return redirect("/prestamos")->with("error", "No se pudo realizar el préstamo. ".$mensaje);
        }
        else{
            $prestamo= new Prestamos();
            $prestamo->id_lector= $idLector;
            $prestamo->id_ejemplar= $idEjemplar;
            $prestamo->fecha_prestamo= date('Y-m-d H:i:s');
            $prestamo->fecha_devolucion = $fecha_devolucion;
            $prestamo->id_tipo_identificacion= $identificacion;
            $prestamo->creado_por = auth()->user()->id;
            $prestamo->modificado_por = auth()->user()->id;
            $prestamo->save();
            //Se cambia el estatus del ejemplar a prestado
            $ejemplar->id_estatus=1;
            $ejemplar->modificado_por = auth()->user()->id;
            $ejemplar->save();
            BitacoraController::saveBitacora("Realiza préstamo a ".$lector->nombre." ".$lector->apellidos." con id: ".$lector->id." ejemplar: ".$ejemplar->codigo);
            return redirect("/prestamos")->with("success", "Préstamo realizado correctamente, ejemplar: ".$ejemplar->codigo." a ".$lector->nombre." ".$lector->apellidos);
        }
    }

    function devolver(Request $request){
        $id= $request->dev_id;
        $prestamo= Prestamos::find($id);
        if($prestamo){
            $prestamo->fecha_devolucion_real = date('Y-m-d H:i:s');
            $prestamo->modificado_por = auth()->user()->id;
            $prestamo->save();
            //Se cambia el estatus del ejemplar a disponible
            $ejemplar= Ejemplares::find($prestamo->id_ejemplar);
            $ejemplar->id_estatus=3;
            $ejemplar->modificado_por = auth()->user()->id;
            $ejemplar->save();
            BitacoraController::saveBitacora("Realiza la devolución del libro ".$ejemplar->codigo);
            return redirect("/prestamos")->with("success", "Devolución realizada correctamente");
        }
        else{
            return redirect("/prestamos")->with("error", "No se pudo hacer la devolución del libro.");
        }
    }

    function show($fecha_prestamo_ini,$fecha_prestamo_fin,$fecha_devolucion,$id_sucursal,$excede="N"){
        $where_array=[["fecha_devolucion_real",$fecha_devolucion]];
        if($fecha_prestamo_ini != null && $fecha_prestamo_fin !=null){
            array_push($where_array,["fecha_prestamo",">=","$fecha_prestamo_ini 00:00:00"]);
            array_push($where_array,["fecha_prestamo","<=","$fecha_prestamo_fin 23:59:59"]);
        }
        if($id_sucursal != null){
            array_push($where_array,["id_sucursal","=","$id_sucursal"]);
        }
        if($excede=="Y"){
            unset($where_array[0]);
            array_push($where_array,[\DB::raw("TIMESTAMPDIFF(DAY, fecha_devolucion,fecha_devolucion_real) "),">","0"]);
        }
        if($excede=="T"){
            unset($where_array[0]);
        }
        $prestamos= Prestamos::select("prestamos.id", "id_lector","matricula","id_ejemplar", "fecha_prestamo",\DB::raw("date_format(fecha_prestamo, '%d/%m/%Y') as fecha_prestamo2"),"fecha_devolucion",\DB::raw("date_format(fecha_devolucion, '%d/%m/%Y') as fecha_devolucion2"),\DB::raw("TIMESTAMPDIFF(DAY, fecha_devolucion, CURDATE()) as diferencia_dias"),\DB::raw("TIMESTAMPDIFF(DAY, fecha_devolucion, fecha_devolucion_real) as diferencia_entrega"),"id_estatus","fecha_devolucion_real",\DB::raw("date_format(fecha_devolucion_real, '%d/%m/%Y') as fecha_devolucion_real2"), "nombre","apellidos", "codigo","titulo","identificacion")
        ->join("lectores","lectores.id","=","prestamos.id_lector")
        ->join("ejemplares","ejemplares.id","=","prestamos.id_ejemplar")
        ->join("libros","libros.id","=","ejemplares.id_libro")
        ->join("tipo_identificacion","tipo_identificacion.id","=","prestamos.id_tipo_identificacion")
        ->where($where_array)
        ->orderBy("fecha_prestamo","desc")
        ->get();
        return $prestamos;
    }

    function librosNoPrestados($sucursal){
        $where_array=($sucursal == 0) ? []: [["id_sucursal",$sucursal]];

        $prestamos= Prestamos::select("libros.id")
        ->join("ejemplares","ejemplares.id","=","prestamos.id_ejemplar")
        ->join("libros","libros.id","=","ejemplares.id_libro")
        ->where($where_array)
        ->get();

        $noPrestados= Libros::select("isbn","titulo")
        ->whereNotIn("id",$prestamos)
        ->get();
        return $noPrestados;
    }
}
