<?php

namespace App\Http\Controllers;
use App\Models\Facultades;
use App\Models\Lectores;
use Illuminate\Http\Request;

class FacultadesController extends Controller{
    function index(){
        $top = Facultades::selectRaw("facultad, COUNT(id_facultad) total")
            ->join("lectores","lectores.id_facultad","=","facultades.id")
            ->orderByDesc('total')
            ->groupBy('facultad')
            ->limit(10)
            ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de facultades");
        $facultades = facultades::orderBy('facultad')->get();
        return view('facultades',compact('facultades','top'));
    }

    function store(Request $request){
        $nombre= trim($request->facultad);
        if(facultades::where("facultad", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear una nueva facultad: ".$nombre." pero no se pudo debido a que ya existe otra facultad con el mismo nombre");
            return redirect("/facultades")->with("error", "Ya existe una facultad con el nombre ".$nombre);
        }
        else{
            $facultad = new facultades();
            $facultad->facultad= $nombre;
            $facultad->creado_por = auth()->user()->id;
            $facultad->modificado_por = auth()->user()->id;
            $facultad->save();
            BitacoraController::saveBitacora("Crea una nueva facultad: ".$nombre);
            return redirect("/facultades")->with("success", "Facultad ".$nombre." creada");
        }
    }

    function update(Request $request){
        $id= $request->idfacultad;
        $nombre= trim($request->facultad);
        $facultad= facultades::find($id);
        if($facultad){
            if(facultades::where("facultad", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar la facultad: ".$nombre." pero no se pudo debido a que ya existe otra facultad con el mismo nombre");
                return redirect("/facultades")->with("error", "No se puede modificar debido a que ya existe una facultad con el nombre ".$nombre);
            }
            else{
                $facultad->facultad=$nombre;
                $facultad->modificado_por = auth()->user()->id;
                $facultad->save();
                BitacoraController::saveBitacora("Modifica la facultad: ".$nombre);
                return redirect("/facultades")->with("success", "Facultad modificada ".$nombre);
            }
        }
        else{
            return redirect("/facultades")->with("error", "No existe la facultad ");
        }
        
    }
    function delete(Request $request){
        $id= $request->idFac;
        $facultad= facultades::find($id);
        if($facultad){
            $nombre= $facultad->facultad;
            if(Lectores::where("id_facultad",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar la facultad: ".$nombre." pero no se pudo debido a que hay letores que pertenecen a esa faultad");
                return redirect("/facultades")->with("error", "No se puede eliminar la facultad debido a que ya existen lectores que pertenecen a esa facultad");
            }
            else{
                $facultad->destroy($id);
                BitacoraController::saveBitacora("Elimina la facultad: ".$nombre);
                return redirect("/facultades")->with("success", "Facultad eliminada ");
            }
        }
        else{
            return redirect("/facultades")->with("error", "No existe la facultad ");
        }
    }
}
