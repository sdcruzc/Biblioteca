<?php

namespace App\Http\Controllers;
use App\Models\Campus;
use App\Models\Lectores;
use Illuminate\Http\Request;

class CampusController extends Controller{
    function index(){
        $top = Campus::selectRaw("campus, COUNT(id_campus) total")
            ->join("lectores","lectores.id_campus","=","campus.id")
            ->orderByDesc('total')
            ->groupBy('campus')
            ->limit(10)
            ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de campus");
        $campus = Campus::orderBy('campus')->get();
        return view('campus',compact('campus','top'));
    }

    function store(Request $request){
        $nombre= trim($request->campus);
        if(Campus::where("campus", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear un nuevo campus: ".$nombre." pero no se pudo debido a que ya existe otro campus con el mismo nombre");
            return redirect("/campus")->with("error", "Ya existe un campus con el nombre ".$nombre);
        }
        else{
            $campus = new Campus();
            $campus->campus= $nombre;
            $campus->creado_por = auth()->user()->id;
            $campus->modificado_por = auth()->user()->id;
            $campus->save();
            BitacoraController::saveBitacora("Crea un nuevo campus: ".$nombre);
            return redirect("/campus")->with("success", "Campus ".$nombre." creado");
        }
    }

    function update(Request $request){
        $id= $request->idcampus;
        $nombre= trim($request->campus);
        $campus= campus::find($id);
        if($campus){
            if(campus::where("campus", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar el campus: ".$nombre." pero no se pudo debido a que ya existe otro campus con el mismo nombre");
                return redirect("/campus")->with("error", "No se puede modificar debido a que ya existe un campus con el nombre ".$nombre);
            }
            else{
                $campus->campus=$nombre;
                $campus->modificado_por = auth()->user()->id;
                $campus->save();
                BitacoraController::saveBitacora("Modifica el campus: ".$nombre);
                return redirect("/campus")->with("success", "Campus modificado ".$nombre);
            }
        }
        else{
            return redirect("/campus")->with("error", "No existe el campus ");
        }
        
    }
    function delete(Request $request){
        $id= $request->idCam;
        $campus= Campus::find($id);
        if($campus){
            $nombre= $campus->campus;
            if(Lectores::where("id_campus",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar el campus: ".$nombre." pero no se pudo debido a que hay letores que pertenecen a ese campus");
                return redirect("/campus")->with("error", "No se puede eliminar el campus debido a que ya existen lectores que pertenecen a ese campus");
            }
            else{
                $campus->destroy($id);
                BitacoraController::saveBitacora("Elimina el campus: ".$nombre);
                return redirect("/campus")->with("success", "Campus eliminado ");
            }
        }
        else{
            return redirect("/campus")->with("error", "No existe el campus ");
        }
    }
}
