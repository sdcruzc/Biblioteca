<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportesController extends Controller{
    function index(){
        BitacoraController::saveBitacora("Ingresa al módulo de reportes");
        return view('reportes');
    }
    function show(Request $request){
        $op= $request->op;
        $usuario= auth()->user()->id;
        $home = new HomeController();
        $info_suc = $home->getSucursalUsuario($usuario);
        $sucursal= (count($info_suc)>0)? $info_suc[0]->id_sucursal:0;
        if($op==1){
            $ejemplares = new EjemplaresController();
            $resultado = $ejemplares->getInventario($sucursal);

        }
        else if($op==2){
            $ejemplares = new LibrosController();
            $resultado = $ejemplares->getTopPrestamos($sucursal);
        }
        else if($op==3){
            $prestamos = new PrestamosController();
            $resultado = $prestamos->librosNoPrestados($sucursal);
        }
        else if($op==4){
            $prestamos = new PrestamosController();
            $resultado = $prestamos->show(null,null,null,$sucursal);
        }
        else if($op==5){
            $prestamos = new PrestamosController();
            $resultado = $prestamos->show(null,null,null,$sucursal,"Y");
        }
        else if($op==6){
            $inicio= $request->inicio;
            $fin= $request->fin;
            $prestamos = new PrestamosController();
            $resultado = $prestamos->show($inicio,$fin,null,$sucursal,"T");
        }
        return $resultado;
    }
}
