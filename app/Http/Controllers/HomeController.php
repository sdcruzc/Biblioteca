<?php

namespace App\Http\Controllers;
use App\Models\Autores;
use App\Models\Editoriales;
use App\Models\Categorias;
use App\Models\User;
use App\Models\Bitacora;
use App\Models\Lectores;
use App\Models\Libros;
use App\Models\Facultades;
use App\Models\Ejemplares;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $usuario= auth()->user()->id;
        $sucursales= $this->getSucursalUsuario($usuario);
        $bitacora= Bitacora::selectRaw("fecha, DATE_FORMAT(fecha, '%d/%m/%Y') as acceso_fecha, DATE_FORMAT(fecha, '%h:%i') as acceso_hora")
        ->where("id_usuario",auth()->user()->id)
        ->where("accion","Inicia sesión")
        ->orderBy("fecha","DESC")
        ->limit(1)
        ->get();
        $sucursal= (count($sucursales)>0) ? $sucursales[0]->sucursal : "Sin sucursal";
        $fecha= (count($sucursales)>0) ? $sucursales[0]->fecha : "";
        $acceso_fecha= (count($bitacora)>0) ? $bitacora[0]->acceso_fecha : "";
        $acceso_hora= (count($bitacora)>0) ? $bitacora[0]->acceso_hora : "";

        $autores = Autores::count();
        $editoriales = Editoriales::count();
        $categorias = Categorias::count();
        $lectores = Lectores::count();
        $facultades = Facultades::count();
        $prestamosController = new PrestamosController();
        $misucursal= (count($this->getSucursalUsuario($usuario))>0)? $this->getSucursalUsuario($usuario)[0]->id_sucursal:0;
        $where_sucursal=($misucursal==0)? [] : [['id_sucursal','=',$misucursal]];

        $libros = Ejemplares::where($where_sucursal)->count();
        $resultadoPrestamos = $prestamosController->show(null,null,null,$misucursal);
        $prestamos= count($resultadoPrestamos);
        BitacoraController::saveBitacora("Inicia sesión");
        return view('home',compact('autores','editoriales','categorias','sucursal','lectores','libros','facultades','fecha','acceso_fecha','acceso_hora','prestamos'));
    }
    function getSucursalUsuario($usuario){
        $sucursales= User::select("users.id","users.name","email","sucursal","id_sucursal",DB::raw("DATE_FORMAT(users.created_at, '%d/%m/%Y') as fecha"))
        ->leftJoin("usuarios_sucursales","users.id","=","usuarios_sucursales.id_usuario")
        ->leftJoin("sucursales","usuarios_sucursales.id_sucursal","=","sucursales.id")
        ->where("users.id",$usuario)
        ->get();
        return $sucursales;
    }
}
