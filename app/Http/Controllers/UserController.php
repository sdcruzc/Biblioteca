<?php

namespace App\Http\Controllers;
use DB;
use App\Models\User;
use App\Models\Sucursales;
use App\Models\Usuarios_sucursales;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use Illuminate\Http\Request;

class UserController extends Controller{
    function index(){
        BitacoraController::saveBitacora("Ingresa al módulo de usuarios");
        $sucursales= Sucursales::all();
        $idRolUser=(Auth()->user()->roles[0]->name =="Super Admin") ? 0 : $this->getRolId(auth()->user()->id);

        $roles= Role::all()->where("id",">",$idRolUser);
        $user= User::select("users.id","users.name","email","sucursal","id_sucursal","roles.name as rol")
        ->join("model_has_roles","users.id","=","model_has_roles.model_id")
        ->join("roles","model_has_roles.role_id","=","roles.id")
        ->leftJoin("usuarios_sucursales","users.id","=","usuarios_sucursales.id_usuario")
        ->leftJoin("sucursales","usuarios_sucursales.id_sucursal","=","sucursales.id")
        ->where("roles.id",">",$idRolUser)
        ->get();
        return view('usuarios',compact('user','sucursales','roles'));
    }

    function store(Request $request){
        $nombre= trim($request->nombre);
        $email= trim($request->email);
        $pass= Hash::make(trim($request->pass));
        $sucursal= $request->sucursal;
        $rol= $request->rol;

        if(User::where("email", $email)->exists()){
            BitacoraController::saveBitacora("Intenta crear un nuevo usuario: ".$email." pero no se pudo debido a que ya existe otro usuario con el mismo correo");
            return redirect("/usuarios")->with("error", "Ya existe un usuario con el correo ".$email);
        }
        else{
            //Crear usuario
            $user = new User();
            $user->name= $nombre;
            $user->email= $email;
            $user->password= $pass;
            $user->save();
            $iduser = $user->id;
            //Asignar usuario a sucursal
            $user_sucursal= new Usuarios_sucursales();
            $user_sucursal->id_usuario= $iduser;
            $user_sucursal->id_sucursal= $sucursal;
            $user_sucursal->creado_por = auth()->user()->id;
            $user_sucursal->modificado_por = auth()->user()->id;
            $user_sucursal->save();
            //Asignar rol al usuario
            $user->assignRole($rol);
            BitacoraController::saveBitacora("Crea una nuevo usuario: ".$email." con rol ".$rol);
            //Envío de correo
            $enviado=="N";
            $enable= env('ENABLE_SEND_MAIL', 'NO');
            if($enable=="YES"){
                $enviado=$this->enviarCorreo("Creación de cuenta",$request,"Se te ha creado un nuevo acceso para ingresar al sistema.");
            }
            if($enviado=="Y"){
                return redirect("/usuarios")->with("success", "Usuario ".$email." con rol ".$rol." creado. Se envió el acceso al correo del usuario.");
            }
            else{
                return redirect("/usuarios")->with("warning", "Usuario ".$email." con rol ".$rol." creado. NO se pudo enviar el acceso por correo");
            }
        }
    }

    function update(Request $request){
        $id= $request->idusuario;
        $nombre= trim($request->nombre);
        $sucursal= $request->sucursal;
        $rol= $request->rol;
        $user= User::find($id);
        if($user){
            $user->name= $nombre;
            $user->save();
            //Modificar sucursal del usuario
            DB::connection()->update( DB::raw("UPDATE usuarios_sucursales SET id_sucursal=? WHERE id_usuario= ? "),[$sucursal,$id]);
            //Modificar el rol
            $user->syncRoles($rol);
            BitacoraController::saveBitacora("Modifica el usuario: ".$nombre." rol ".$rol);
            return redirect("/usuarios")->with("success", "Usuario modificado ".$nombre);
        }
        else{
            return redirect("/usuarios")->with("error", "No existe el usuario ");
        }
        
    }
    function enviarCorreo($title,$request,$texto){
        try{
            $email = $request->email;
            Mail::to($email)->send(new SendEmail($request,$title,$texto));
            return "Y";
        }
        catch(Throwable $ex){
            return "N";
        }
    }
    function updatePassword(Request $request){
        $id = $request->iduser;
        $pass1 = $request->pass1;
        $pass2 = $request->pass2;
        if($pass1==$pass2){
            $res= $this->changePassword($id,$pass1);
            if($res=="OK"){
                return redirect(url()->previous())->with("success", "Contraseña actualizada");
            }
        }
        else{
            return redirect(url()->previous())->with("error", "Debes escribir la contraseña y repetirla exactamente igual.");
        }
    }
    function changePassword($id,$pass){
        $msj="No existe el usuario";
        $user= User::find($id);
        if($user){
            $user->password=Hash::make($pass);
            $user->save();
            $msj="OK";
        }
        return $msj;
    }
    function getRolId($userId){
        $info= DB::connection()->select( DB::raw("SELECT role_id FROM model_has_roles WHERE model_id = ? limit 1"),[$userId]);
        $idRol = (count($info)>0) ? $info[0]->role_id : 0;
        return $idRol;
    }
}