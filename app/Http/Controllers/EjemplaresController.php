<?php

namespace App\Http\Controllers;
use App\Models\Ejemplares;
use App\Models\Estatus_ejemplares;
use App\Models\Prestamos;
use Illuminate\Http\Request;

class EjemplaresController extends Controller{
    function store(Request $request){
        $codigo= trim($request->codigo);
        $pasillo= trim($request->pasillo);
        $estante= trim($request->estante);
        $sucursal= $request->sucursal;
        $estatus= $request->estatus;
        $libro= $request->libro;
        if(Ejemplares::where("codigo", $codigo)->where("id_sucursal", $sucursal)->exists()){
            BitacoraController::saveBitacora("Intenta crear un nuevo ejemplar: ".$codigo." pero no se pudo debido a que ya existe otro ejemplar con el mismo código en la sucursal ".$sucursal);
            return redirect("libros/".$libro)->with("error", "Ya existe un ejemplar con el codigo ".$codigo);
        }
        else{
            $ejemplar = new Ejemplares();
            $ejemplar->codigo= $codigo;
            $ejemplar->pasillo= $pasillo;
            $ejemplar->estante= $estante;
            $ejemplar->id_libro= $libro;
            $ejemplar->id_sucursal= $sucursal;
            $ejemplar->id_estatus= $estatus;
            $ejemplar->creado_por = auth()->user()->id;
            $ejemplar->modificado_por = auth()->user()->id;
            $ejemplar->save();
            BitacoraController::saveBitacora("Crea una nuevo ejemplar: ".$codigo." en sucursal ".$sucursal);
            return redirect("libros/".$libro)->with("success", "Ejemplar creado ".$codigo);
        }
    }

    function update(Request $request){
        $id= $request->idejemplar;
        $codigo= trim($request->codigo);
        $pasillo= trim($request->pasillo);
        $estante= trim($request->estante);
        $sucursal= $request->sucursal;
        $estatus= $request->estatus;
        $libro= $request->libro;
        $ejemplar= Ejemplares::find($id);
        if($ejemplar){
            if(Ejemplares::where("codigo", $codigo)->where("id_sucursal", $sucursal)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar el ejemplar: ".$codigo." pero no se pudo debido a que ya existe otro ejemplar con el mismo código en la sucursal ".$sucursal);
                return redirect("libros/".$libro)->with("error", "No se puede modificar debido a que ya existe un ejemplar con el código ".$codigo." en la sucursal.");
            }
            else{
                $ejemplar->pasillo= $pasillo;
                $ejemplar->estante= $estante;
                $ejemplar->id_sucursal= $sucursal;
                $ejemplar->id_estatus= $estatus;
                $ejemplar->modificado_por = auth()->user()->id;
                $ejemplar->save();
                BitacoraController::saveBitacora("Modifica el ejemplar: ".$codigo." pasillo: ".$pasillo." , estante: ".$estante." , sucursal: ".$sucursal." , estatus: ".$estatus);
                return redirect("libros/".$libro)->with("success", "Ejemplar modificado ".$codigo);
            }
        }
        else{
            return redirect("libros/".$libro)->with("error", "No existe el ejemplar ");
        }
    }

    function delete(Request $request){
        $id= $request->idEje;
        $libro= $request->libro;
        $ejemplar= Ejemplares::find($id);
        if($ejemplar){
            $ejemplar->id_estatus= 2;
            $ejemplar->modificado_por = auth()->user()->id;
            $ejemplar->save();
            BitacoraController::saveBitacora("Da de baja el ejemplar: ".$id." código: ".$ejemplar->codigo." sucursal: ".$ejemplar->id_sucursal);
            return redirect("libros/".$libro)->with("success", "Ejemplar dado de baja ");
            
        }
        else{
            return redirect("libros/".$libro)->with("error", "No existe el ejemplar");
        }

    }

    function show(Request $request){
        $codigo= $request->codigo;
        $sucursal= $request->sucursal; 
        $status = $request->status;
        $where_array=($status == null || $status =="")? [["codigo",$codigo],["id_sucursal",$sucursal]]:[["codigo",$codigo],["id_sucursal",$sucursal],["id_estatus",$status]];
        $ejemplar= Ejemplares::select("ejemplares.id","isbn","titulo","id_sucursal","id_estatus")
        ->where($where_array)
        ->join("libros","libros.id","=","ejemplares.id_libro")
        ->limit(1)->get();
        return $ejemplar;
    }

    function getInventario($sucursal){
        $where_array=($sucursal == 0) ? []: [["id_sucursal",$sucursal]];

        $ejemplares= Ejemplares::select("ejemplares.id","isbn","titulo","codigo","id_sucursal","estatus","pasillo","estante")
        ->join("libros","libros.id","=","ejemplares.id_libro")
        ->join("estatus_ejemplar","estatus_ejemplar.id","=","ejemplares.id_estatus")
        ->where($where_array)
        ->get();
        return $ejemplares;
    }
}