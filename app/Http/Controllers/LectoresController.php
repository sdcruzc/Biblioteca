<?php

namespace App\Http\Controllers;
use App\Models\Lectores;
use App\Models\Campus;
use App\Models\Facultades;
use App\Models\Tipo_lector;
use App\Models\Prestamos;
use Illuminate\Http\Request;

class LectoresController extends Controller{
    function index(){
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de lectores");
        //$lectores = Lectores::orderBy('nombre')->get();


        $lectores = Lectores::select("lectores.id","matricula","nombre","apellidos","curp","celular","telefono","correo","direccion","campus","facultad","tipo_lector","fecha_nacimiento","id_campus","id_facultad","id_tipo_lector",\DB::raw("DATE_FORMAT(fecha_nacimiento, '%d/%m/%Y' ) AS nacimiento"))
        ->join("campus","lectores.id_campus","=","campus.id")
        ->join("facultades","lectores.id_facultad","=","facultades.id")
        ->join("tipo_lector","lectores.id_tipo_lector","=","tipo_lector.id")
        ->orderBy('nombre')
        ->get();


        $campus = Campus::orderBy('campus')->get();
        $facultades = Facultades::orderBy('facultad')->get();
        $tipos = Tipo_lector::orderBy('tipo_lector')->get();
        return view('lectores',compact('lectores','campus','facultades','tipos'));
    }

    function store(Request $request){
        $matricula= trim($request->matricula);
        $nombre= trim($request->nombre);
        $apellidos= trim($request->apellidos);
        $nacimiento= $request->nacimiento;
        $curp= trim($request->curp);
        $celular= trim($request->celular);
        $telefono= trim($request->telefono);
        $correo= trim($request->correo);
        $direccion= trim($request->direccion);
        $campus = $request->campus;
        $facultad = $request->facultad;
        $tipo = $request->tipo;
        $completo= $matricula." ".$nombre." ".$apellidos;
        if(Lectores::where("nombre", $nombre)->where("apellidos", $apellidos)->where("matricula", $matricula)->exists()){
            BitacoraController::saveBitacora("Intenta crear un nuevo lector: ".$completo." pero no se pudo debido a que ya existe otro lector con el mismo nombre y matrícula");
            return redirect("/lectores")->with("error", "Ya existe un lector con la información ".$completo);
        }
        else{
            $lector = new Lectores();
            $lector->matricula= $matricula;
            $lector->nombre= $nombre;
            $lector->apellidos= $apellidos;
            $lector->fecha_nacimiento= $nacimiento;
            $lector->nombre= $nombre;
            $lector->curp= $curp;
            $lector->celular= $celular;
            $lector->telefono= $telefono;
            $lector->correo= $correo;
            $lector->direccion= $direccion;
            $lector->id_campus= $campus;
            $lector->id_facultad= $facultad;
            $lector->id_tipo_lector= $tipo;
            $lector->creado_por = auth()->user()->id;
            $lector->modificado_por = auth()->user()->id;
            $lector->save();
            BitacoraController::saveBitacora("Crea un nuevo lector: ".$nombre);
            return redirect("/lectores")->with("success", "Lector ".$nombre." ".$apellidos." creado");
        }
    }

    function update(Request $request){
        $id= $request->idlector;
        $matricula= trim($request->matricula);
        $nombre= trim($request->nombre);
        $apellidos= trim($request->apellidos);
        $nacimiento= $request->nacimiento;
        $curp= trim($request->curp);
        $celular= trim($request->celular);
        $telefono= trim($request->telefono);
        $correo= trim($request->correo);
        $direccion= trim($request->direccion);
        $campus = $request->campus;
        $facultad = $request->facultad;
        $tipo = $request->tipo;
        $completo= $matricula." ".$nombre." ".$apellidos;
        $lector= Lectores::find($id);
        if($lector){
            if(Lectores::where("nombre", $nombre)->where("apellidos", $apellidos)->where("matricula", $matricula)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar al lector: ".$nombre." pero no se pudo debido a que ya existe otro con el mismo nombre y matrícula");
                return redirect("/lectores")->with("error", "No se puede modificar debido a que ya existe un lector con el nombre y matrícula".$nombre);
            }
            else{
                $lector->matricula= $matricula;
                $lector->nombre= $nombre;
                $lector->apellidos= $apellidos;
                $lector->fecha_nacimiento= $nacimiento;
                $lector->nombre= $nombre;
                $lector->curp= $curp;
                $lector->celular= $celular;
                $lector->telefono= $telefono;
                $lector->correo= $correo;
                $lector->direccion= $direccion;
                $lector->id_campus= $campus;
                $lector->id_facultad= $facultad;
                $lector->id_tipo_lector= $tipo;
                $lector->modificado_por = auth()->user()->id;
                $lector->save();
                BitacoraController::saveBitacora("Modifica al lector: ".$completo);
                return redirect("/lectores")->with("success", "Lector modificado ".$completo);
            }
        }
        else{
            return redirect("/lectores")->with("error", "No existe el lector ");
        }
    }

    function delete(Request $request){
        $id= $request->idLec;
        $lector= Lectores::find($id);
        if($lector){
            $completo= $lector->matricula." ".$lector->nombre." ".$lector->apellidos;
            if(Prestamos::where("id_lector",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar al lector: ".$completo." pero no se pudo debido a que tiene registrado préstamos");
                return redirect("/lectores")->with("error", "No se puede eliminar al lector debido a que ya existen préstamos asociados al lector");
            }
            else{
                $lector->destroy($id);
                BitacoraController::saveBitacora("Elimina al lector: ".$completo);
                return redirect("/lectores")->with("success", "Lector eliminado ");
            }
        }
        else{
            return redirect("/lectores")->with("error", "No existe el lector ");
        }
    }

    function show(Request $request){
        $matricula= $request->matricula;
        $lector= Lectores::where("matricula",$matricula)->limit(1)->get();
        return $lector;
    }
}
