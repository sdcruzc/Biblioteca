<?php

namespace App\Http\Controllers;
use App\Models\Tipo_lector;
use App\Models\Lectores;
use Illuminate\Http\Request;

class TipoLectorController extends Controller{
    function index(){
        $top = Tipo_lector::selectRaw("tipo_lector, COUNT(id_tipo_lector) total")
            ->join("lectores","lectores.id_tipo_lector","=","tipo_lector.id")
            ->orderByDesc('total')
            ->groupBy('tipo_lector')
            ->limit(10)
            ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de tipos de lectores");
        $tipo = Tipo_lector::orderBy('tipo_lector')->get();
        return view('tipos_lectores',compact('tipo','top'));
    }

    function store(Request $request){
        $nombre= trim($request->tipo);
        if(Tipo_lector::where("tipo_lector", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear un nuevo tipo de lector: ".$nombre." pero no se pudo debido a que ya existe otro tipo de lector con el mismo nombre");
            return redirect("/tipos-lectores")->with("error", "Ya existe un tipo de lector con el nombre ".$nombre);
        }
        else{
            $tipo = new Tipo_lector();
            $tipo->tipo_lector= $nombre;
            $tipo->creado_por = auth()->user()->id;
            $tipo->modificado_por = auth()->user()->id;
            $tipo->save();
            BitacoraController::saveBitacora("Crea un nuevo de lector: ".$nombre);
            return redirect("/tipos-lectores")->with("success", "Tipo de lector ".$nombre." creado");
        }
    }

    function update(Request $request){
        $id= $request->idtipo;
        $nombre= trim($request->tipo);
        $tipo= Tipo_lector::find($id);
        if($tipo){
            if(Tipo_lector::where("tipo_lector", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar el campus: ".$nombre." pero no se pudo debido a que ya existe otro tipo de lector con el mismo nombre");
                return redirect("/tipos-lectores")->with("error", "No se puede modificar debido a que ya existe un tipo de lector con el nombre ".$nombre);
            }
            else{
                $tipo->tipo_lector=$nombre;
                $tipo->modificado_por = auth()->user()->id;
                $tipo->save();
                BitacoraController::saveBitacora("Modifica el tipo de lector: ".$nombre);
                return redirect("/tipos-lectores")->with("success", "Tipo de lector modificado ".$nombre);
            }
        }
        else{
            return redirect("/tipos-lectores")->with("error", "No existe el tipo de lector ");
        }
        
    }
    function delete(Request $request){
        $id= $request->idTipo;
        $tipo= Tipo_lector::find($id);
        if($tipo){
            $nombre= $tipo->tipo_lector;
            if(Lectores::where("id_tipo_lector",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar el tipo de lector: ".$nombre." pero no se pudo debido a que hay letores que pertenecen a ese tipo");
                return redirect("/tipos-lectores")->with("error", "No se puede eliminar el tipo de lector debido a que ya existen lectores que pertenecen a ese tipo");
            }
            else{
                $tipo->destroy($id);
                BitacoraController::saveBitacora("Elimina el tipo de lector: ".$nombre);
                return redirect("/tipos-lectores")->with("success", "Tipo de lector eliminado ");
            }
        }
        else{
            return redirect("/tipos-lectores")->with("error", "No existe el tipo de lector ");
        }
    }
}
