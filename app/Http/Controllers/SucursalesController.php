<?php

namespace App\Http\Controllers;
use App\Models\Sucursales;
use App\Models\Usuarios_sucursales;
use Illuminate\Http\Request;

class SucursalesController extends Controller{
    function index(){
        $top = Sucursales::selectRaw("sucursal, COUNT(id_sucursal) total")
            ->join("ejemplares","ejemplares.id_sucursal","=","sucursales.id")
            ->join("prestamos","prestamos.id_ejemplar","=","ejemplares.id")
            ->orderByDesc('total')
            ->groupBy('sucursal')
            ->limit(10)
            ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de sucursales");
        $sucursales = Sucursales::orderBy('sucursal')->get();
        return view('sucursales',compact('sucursales','top'));
    }

    function store(Request $request){
        $nombre= trim($request->sucursal);
        if(Sucursales::where("sucursal", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear una nueva sucursal: ".$nombre." pero no se pudo debido a que ya existe otra sucursal con el mismo nombre");
            return redirect("/sucursales")->with("error", "Ya existe una sucursal con el nombre ".$nombre);
        }
        else{
            $sucursales = new Sucursales();
            $sucursales->sucursal= $nombre;
            $sucursales->creado_por = auth()->user()->id;
            $sucursales->modificado_por = auth()->user()->id;
            $sucursales->save();
            BitacoraController::saveBitacora("Crea una nueva sucursal: ".$nombre);
            return redirect("/sucursales")->with("success", "Sucursal ".$nombre." creada");
        }
    }

    function update(Request $request){
        $id= $request->idsucursal;
        $nombre= trim($request->sucursal);
        $sucursales= Sucursales::find($id);
        if($sucursales){
            if(Sucursales::where("sucursal", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar la sucursal: ".$nombre." pero no se pudo debido a que ya existe otra sucursal con el mismo nombre");
                return redirect("/sucursales")->with("error", "No se puede modificar debido a que ya existe una sucursale con el nombre ".$nombre);
            }
            else{
                $sucursales->sucursal=$nombre;
                $sucursales->modificado_por = auth()->user()->id;
                $sucursales->save();
                BitacoraController::saveBitacora("Modifica la sucursal: ".$nombre);
                return redirect("/sucursales")->with("success", "Sucursal modificado ".$nombre);
            }
        }
        else{
            return redirect("/sucursales")->with("error", "No existe la sucursal ");
        }
        
    }
    function delete(Request $request){
        $id= $request->idSuc;
        $sucursales= Sucursales::find($id);
        if($sucursales){
            $nombre= $sucursales->sucursal;
            if(Usuarios_sucursales::where("id_sucursal",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar la sucursal: ".$nombre." pero no se pudo debido a que hay usuarios que pertenecen a esa sucursal");
                return redirect("/sucursales")->with("error", "No se puede eliminar la sucursal debido a que ya existen usuarios que pertenecen a esa sucursal");
            }
            else{
                $sucursales->destroy($id);
                BitacoraController::saveBitacora("Elimina la sucursal: ".$nombre);
                return redirect("/sucursales")->with("success", "Sucursal eliminada ");
            }
        }
        else{
            return redirect("/sucursales")->with("error", "No existe la sucursal ");
        }
    }
}
