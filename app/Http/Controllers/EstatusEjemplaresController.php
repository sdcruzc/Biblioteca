<?php

namespace App\Http\Controllers;
use App\Models\Estatus_ejemplares;
use App\Models\Ejemplares;
use Illuminate\Http\Request;

class EstatusEjemplaresController extends Controller{
    function index(){
        $top = Estatus_ejemplares::selectRaw("estatus, COUNT(id_estatus) total")
                ->join("ejemplares","ejemplares.id_estatus","=","estatus_ejemplar.id")
                ->orderByDesc('total')
                ->groupBy('estatus')
                ->limit(10)
                ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de estatus de ejemplares");
        $estatus = Estatus_ejemplares::orderBy('estatus')->get();
        return view('estatus_ejemplares',compact('estatus','top'));
    }

    function store(Request $request){
        $nombre= trim($request->estatus);
        if(Estatus_ejemplares::where("estatus", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear una nuevo estatus de ejemplar: ".$nombre." pero no se pudo debido a que ya existe otro estatus con el mismo nombre");
            return redirect("/estatus-ejemplares")->with("error", "Ya existe una estatus con el nombre ".$nombre);
        }
        else{
            $estatus = new Estatus_ejemplares();
            $estatus->estatus= $nombre;
            $estatus->creado_por = auth()->user()->id;
            $estatus->modificado_por = auth()->user()->id;
            $estatus->save();
            BitacoraController::saveBitacora("Crea un nuevo estatus: ".$nombre);
            return redirect("/estatus-ejemplares")->with("success", "Estatus ".$nombre." creado");
        }
    }

    function update(Request $request){
        $id= $request->idestatus;
        $nombre= trim($request->estatus);
        $estatus= Estatus_ejemplares::find($id);
        if($estatus){
            if(Estatus_ejemplares::where("estatus", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar el estatus: ".$nombre." pero no se pudo debido a que ya existe otro estatus con el mismo nombre");
                return redirect("/estatus-ejemplares")->with("error", "No se puede modificar debido a que ya existe un estatus con el nombre ".$nombre);
            }
            else{
                $estatus->estatus=$nombre;
                $estatus->modificado_por = auth()->user()->id;
                $estatus->save();
                BitacoraController::saveBitacora("Modifica el estatus: ".$nombre);
                return redirect("/estatus-ejemplares")->with("success", "Estatus modificado ".$nombre);
            }
        }
        else{
            return redirect("/estatus-ejemplares")->with("error", "No existe el estatus ");
        }   
    }

    function delete(Request $request){
        $id= $request->idEst;
        $estatus= Estatus_ejemplares::find($id);
        if($estatus){
            $nombre= $estatus->estatus;
            if(Ejemplares::where("id_estatus",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar el estatus: ".$nombre." pero no se pudo debido a que ya se usa en algún ejemplar");
                return redirect("/estatus-ejemplares")->with("error", "No se puede eliminar el estatus debido a que ya existen ejemplares con ese estatus");
            }
            else{
                $estatus->destroy($id);
                BitacoraController::saveBitacora("Elimina el estatus: ".$nombre);
                return redirect("/estatus-ejemplares")->with("success", "Estatus eliminado ");
            }
        }
        else{
            return redirect("/estatus-ejemplares")->with("error", "No existe el estatus ");
        }
    }

}
