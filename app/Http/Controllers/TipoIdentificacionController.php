<?php

namespace App\Http\Controllers;
use App\Models\Tipo_identificacion;
use App\Models\Prestamos;
use Illuminate\Http\Request;

class TipoIdentificacionController extends Controller{
    function index(){
        $top = Tipo_identificacion::selectRaw("identificacion, COUNT(id_tipo_identificacion) total")
            ->join("prestamos","prestamos.id_tipo_identificacion","=","tipo_identificacion.id")
            ->orderByDesc('total')
            ->groupBy('identificacion')
            ->limit(10)
            ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de identificaciones");
        $identificaciones = Tipo_identificacion::orderBy('identificacion')->get();
        return view('identificaciones',compact('identificaciones','top'));
    }

    function store(Request $request){
        $nombre= trim($request->identificacion);
        if(Tipo_identificacion::where("identificacion", $nombre)->exists()){
            BitacoraController::saveBitacora("Intenta crear una nueva editorial: ".$nombre." pero no se pudo debido a que ya existe otra editorial con el mismo nombre");
            return redirect("/identificaciones")->with("error", "Ya existe una editorial con el nombre ".$nombre);
        }
        else{
            $identificacion = new Tipo_identificacion();
            $identificacion->identificacion= $nombre;
            $identificacion->creado_por = auth()->user()->id;
            $identificacion->modificado_por = auth()->user()->id;
            $identificacion->save();
            BitacoraController::saveBitacora("Crea una nueva identificacioó: ".$nombre);
            return redirect("/identificaciones")->with("success", "Identificacion ".$nombre." creada");
        }
    }

    function update(Request $request){
        $id= $request->ididentificacion;
        $nombre= trim($request->identificacion);
        $identificacion= Tipo_identificacion::find($id);
        if($identificacion){
            if(Tipo_identificacion::where("identificacion", $nombre)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar la identificación: ".$nombre." pero no se pudo debido a que ya existe otra identificación con el mismo nombre");
                return redirect("/identificaciones")->with("error", "No se puede modificar debido a que ya existe una identificación con el nombre ".$nombre);
            }
            else{
                $identificacion->identificacion=$nombre;
                $identificacion->modificado_por = auth()->user()->id;
                $identificacion->save();
                BitacoraController::saveBitacora("Modifica la identificación: ".$nombre);
                return redirect("/identificaciones")->with("success", "Identificación modificada ".$nombre);
            }
        }
        else{
            return redirect("/identificaciones")->with("error", "No existe la identificación ");
        }
    }

    function delete(Request $request){
        $id= $request->idIden;
        $identificacion= Tipo_identificacion::find($id);
        if($identificacion){
            $nombre= $identificacion->identificacion;
            if(Prestamos::where("id_tipo_identificacion",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar la identificación: ".$nombre." pero no se pudo debido a que ya se usa en algún préstamo");
                return redirect("/identificaciones")->with("error", "No se puede eliminar la identificación debido a que ya existen prestamos con esa identificación");
            }
            else{
                $identificacion->destroy($id);
                BitacoraController::saveBitacora("Elimina la identificación: ".$nombre);
                return redirect("/identificaciones")->with("success", "Identificación eliminada ");
            }
        }
        else{
            return redirect("/identificaciones")->with("error", "No existe la identificación ");
        }
    }
}
