<?php

namespace App\Http\Controllers;
use App\Models\Autores_libros;
use App\Models\Autores;
use Illuminate\Http\Request;

class AutoresController extends Controller{
    function index(){
        $top = Autores::selectRaw("CONCAT(autores.nombre,' ',autores.apellidos) autor, COUNT(id_autor) total")
                ->join("autores_libros","autores_libros.id_autor","=","autores.id")
                ->orderByDesc('total')
                ->groupBy('autor')
                ->limit(10)
                ->get();
        BitacoraController::saveBitacora("Ingresa al módulo de catálogo de autores");
        $autores = Autores::orderBy('apellidos')->get();
        return view('autores',compact('autores','top'));
    }

    function store(Request $request){
        $nombre= trim($request->nombre);
        $apellidos= trim($request->apellidos);
        $completo= $nombre." ".$apellidos;
        if(Autores::where("nombre", $nombre)->where("apellidos", $apellidos)->exists()){
            BitacoraController::saveBitacora("Intenta crear un nuevo autor: ".$completo." pero no se pudo debido a que ya existe otro autor con el mismo nombre");
            return redirect("/autores")->with("error", "Ya existe un autor con el nombre ".$completo);
        }
        else{
            $autor = new Autores();
            $autor->nombre= $nombre;
            $autor->apellidos= $apellidos;
            $autor->creado_por = auth()->user()->id;
            $autor->modificado_por = auth()->user()->id;
            $autor->save();
            BitacoraController::saveBitacora("Crea una nuevo autor: ".$completo);
            return redirect("/autores")->with("success", "Autor ".$completo." creado");
        }
    }

    function update(Request $request){
        $id= $request->idautor;
        $nombre= trim($request->nombre);
        $apellidos= trim($request->apellidos);
        $completo= $nombre." ".$apellidos;
        $autor= Autores::find($id);
        if($autor){
            if(Autores::where("nombre", $nombre)->where("apellidos", $apellidos)->where("id", "<>", $id)->exists()){
                BitacoraController::saveBitacora("Intenta modificar el autor: ".$completo." pero no se pudo debido a que ya existe otro autor con el mismo nombre");
                return redirect("/autores")->with("error", "No se puede modificar debido a que ya existe un autor con el nombre ".$completo);
            }
            else{
                $autor->nombre=$nombre;
                $autor->apellidos=$apellidos;
                $autor->modificado_por = auth()->user()->id;
                $autor->save();
                BitacoraController::saveBitacora("Modifica el autor: ".$completo);
                return redirect("/autores")->with("success", "Autor modificado ".$completo);
            }
        }
        else{
            return redirect("/autores")->with("error", "No existe el autor ");
        }
        
    }

    function delete(Request $request){
        $id= $request->idAut;
        $autor= Autores::find($id);
        if($autor){
            $completo= $autor->nombre." ".$autor->apellidos;
            if(Autores_libros::where("id_autor",$id)->exists()){
                BitacoraController::saveBitacora("Intenta eliminar el autor: ".$completo." pero no se pudo debido a que ya se usa en algún libro");
                return redirect("/autores")->with("error", "No se puede eliminar el autor debido a que ya existen libros con ese autor");
            }
            else{
                $autor->destroy($id);
                BitacoraController::saveBitacora("Elimina el autor: ".$completo);
                return redirect("/autores")->with("success", "Autor eliminado ");
            }
        }
        else{
            return redirect("/autores")->with("error", "No existe el autor ");
        }

    }
}