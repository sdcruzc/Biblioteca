<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable{

    use Queueable, SerializesModels;
    public $subject="Mensaje";
    public $msj;
    public $txt;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message,$asunto,$texto){
        $this->msj= $message;
        $this->subject= $asunto;
        $this->txt= $texto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $remitente = auth()->user()->email;
        $nombre= auth()->user()->name;
        return $this->from($address = $remitente, $name = $nombre)->view('emails.password');
    }
}