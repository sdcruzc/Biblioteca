<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estatus_ejemplares extends Model
{
    use HasFactory;
    protected $table= 'estatus_ejemplar';
}
