$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmautor', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var autor= $(this).attr('data-autor');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar al autor "+autor+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#nombre').val('');
    $('#apellidos').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir autor');
        $('#frmautor').attr('action',window.origin+'/save-autor');
    }
    else{
        $('.modal-title').html('Modificar autor');
        var id= $(btn).attr('data-id');
        var nombre= $(btn).attr('data-nombre');
        var apellidos= $(btn).attr('data-apellidos');
        $('#nombre').val(nombre);
        $('#apellidos').val(apellidos);
        $('#idautor').val(id);
        $('#frmautor').attr('action',window.origin+'/update-autor');
    }
    window.setTimeout(function() {
        $("#nombre").trigger("focus");
    }, 500);
}