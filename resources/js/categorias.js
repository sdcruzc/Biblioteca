$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmCategoria', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var categoria= $(this).attr('data-categoria');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar la categoría "+categoria+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#categoria').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir categor&iacute;a');
        $('#frmCategoria').attr('action',window.origin+'/save-categoria');
    }
    else{
        $('.modal-title').html('Modificar categor&iacute;a');
        var id= $(btn).attr('data-id');
        var categoria= $(btn).attr('data-categoria');
        $('#categoria').val(categoria);
        $('#idCategoria').val(id);
        $('#frmCategoria').attr('action',window.origin+'/update-categoria');
    }
    window.setTimeout(function() {
        $("#categoria").trigger("focus");
    }, 500);
}