$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on('click', '.openModalPrestamo', function(){
    $('#matricula').val('');
    $('#ejemplar').val('');
    $('#lblLector').html(' ');
    $('#lblLibro').html(' ');
    $('#fecha').val('');
    $('#identificacion').val('');
    $('#idLector').val('');
    $('#idEjemplar').val('');
    window.setTimeout(function() {
        $('#matricula').trigger('focus');
    }, 500);
});

$(document).on('blur', '#matricula', function(){
    if($('#matricula').val().trim() !==''){
        getLector($('#matricula').val().trim());
    }
});

$(document).on('keyup', '#matricula', function(e){
    if(e.keyCode === 13) {
        getLector($('#matricula').val().trim());
    }
});

$(document).on('blur', '#codigo', function(){
    if($('#codigo').val().trim() !==''){
        getEjemplar($('#codigo').val().trim());
    }
});

$(document).on('keyup', '#codigo', function(e){
    if(e.keyCode === 13) {
        getEjemplar($('#codigo').val().trim());
    }
});

function getLector(matricula){
    if(matricula.trim() ===''){
        show_alerta('Escribe la matrícula del lector','error','matricula');
    }
    else{
        var nombre="No existe lector con la matrícula indicada";
        $('#idLector').val('');
        var info = {matricula:matricula};
        $.ajax({
            type:'POST',
            url:"show-lector",
            data:info,
            success: function(datos){
                if(datos.length >0){
                    nombre= datos[0].nombre+' '+datos[0].apellidos;
                    $('#idLector').val(datos[0].id);
                    $('#divMsj').addClass('d-none');
                    $('#codigo').trigger('focus');
                }
                else{
                    $('#divMsj').removeClass('d-none');
                    $('#divBtn').addClass('d-none');
                    $('#msj').html('<i class="fa-solid fa-xmark"></i> No se puede llevar a cabo el préstamo, se debe escribir una matrícula correcta.');
                }
                $('#lblLector').html(nombre);
            },
            error: function (jqXHR, textStatus, errorThrown){
                if(jqXHR["responseJSON"].message==="CSRF token mismatch."){
                     show_alerta('El tiempo de espera ha caducado, se tendrá que refrescar la página','warning');
                     setTimeout(function(){location.reload();}, 2000);
                 }
                 else{
                     show_alerta('Error inesperado','error');
                 }
            }
        });
    }
}

function getEjemplar(codigo){
    if(codigo.trim() ===''){
        show_alerta('Escribe el código del ejemplar','error','codigo');
    }
    else{
        var nombre="No existe ejemplar con el código indicado o no está disponible para préstamo";
        $('#idEjemplar').val('');
        var sucursal=$('#sucursal').val();
        var info = {codigo:codigo,sucursal:sucursal,status:3};
        $.ajax({
            type:'POST',
            url:"show-ejemplar",
            data:info,
            success: function(datos){
                if(datos.length >0){
                    nombre= datos[0].titulo;
                    $('#idEjemplar').val(datos[0].id);
                    $('#divMsj').addClass('d-none');
                    $('#divBtn').removeClass('d-none');
                    $('#btnSavePrestamo').attr('type','submit');
                }
                else{
                    $('#divMsj').removeClass('d-none');
                    $('#divBtn').addClass('d-none');
                    $('#msj').html('<i class="fa-solid fa-xmark"></i> No se puede llevar a cabo el préstamo, se debe escribir un código de ejemplar válido.');
                }
                $('#lblLibro').html(nombre);
            },
            error: function (jqXHR, textStatus, errorThrown){
                if(jqXHR["responseJSON"].message==="CSRF token mismatch."){
                     show_alerta('El tiempo de espera ha caducado, se tendrá que refrescar la página','warning');
                     setTimeout(function(){location.reload();}, 2000);
                 }
                 else{
                     show_alerta('Error inesperado','error');
                 }
            }
        });
    }
}

$(document).on('click', '.btnDevolver', function(){
    var id= $(this).attr('data-id');
    var codigo= $(this).attr('data-codigo');
    var libro= $(this).attr('data-titulo');
    var lector= $(this).attr('data-nombre');
    $('#dev_id').val(id);
    var form= "frm_devolucion";
    var idboton= "noexiste";
    var titulo='¿'+lector+' va a devolver el libro '+codigo+' '+libro+'?';
    var msj="El libro volverá a estar disponible para préstamo";
    var btn="<i class='fa-solid fa-check'></i> Devolver";
    confirmacion(form,titulo,msj,btn,idboton);

});