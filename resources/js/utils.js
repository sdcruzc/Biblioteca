$(function(){
    var url= location.pathname;
    var partes= url.split('/');
    var ruta= partes[1];
    var rutas=['autores','campus','categorias','estatus-ejemplares','editoriales','facultades','identificaciones','tipos-lectores','sucursales'];
    if(jQuery.inArray(ruta, rutas) > -1) {
        $("#btn-toggle-catalogos").trigger("click");
    }
});
$(function(){
    window.setTimeout(function() {
        $(".alert-fade").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 5000);
});
$(function () {
    $(".footable").footable();
    $(".footable-p-f").footable({
        "paging": {
                "enabled": true
        },
        "filtering": {
            "enabled": true
        },
        "sorting": {
                "enabled": false
        }
    });
    $(".footable-f").footable({
        "paging": {
                "enabled": false
        },
        "filtering": {
            "enabled": true
        },
        "sorting": {
                "enabled": false
        }
    });
    $(".footable-p-f-s").footable({
        "paging": {
                "enabled": true
        },
        "filtering": {
            "enabled": true
        },
        "sorting": {
                "enabled": true
        }
    });
    $(".footable-p-f-s-20").footable({
        "paging": {
                "enabled": false,"current":20
        },
        "filtering": {
            "enabled": true
        },
        "sorting": {
                "enabled": true
        }
    });
});
window.footblepf= function(){
    $(".footable-p-f").footable({
        "paging": {
                "enabled": true
        },
        "filtering": {
            "enabled": true
        },
        "sorting": {
                "enabled": false
        }
    });
}
window.footblepfs= function(){
    $(".footable-p-f-s").footable({
        "paging": {
                "enabled": true
        },
        "filtering": {
            "enabled": true
        },
        "sorting": {
                "enabled": true
        }
    });
}
//Alertas
window.show_alerta= function(mensaje,icono,foco){
    if(foco !==""){
        $("#"+foco).trigger("focus");
    }
    Swal.fire({
        title: mensaje,
        icon: icono,
        customClass: {
            confirmButton: "btn btn-theme",
            popup: 'animated zoomIn'
        },
        buttonsStyling: false
    });
}

window.confirmacion = function(form,titulo,msj,btn,idboton){
    Swal.fire({
        title: titulo,
        text: msj,
        icon: "question",
        showCancelButton: true,
        confirmButtonText: btn,
        cancelButtonText: "<i class='fa-solid fa-ban'></i> No",
        customClass: {
            confirmButton: "btn btn-success me-4",
            cancelButton: "btn btn-danger",
            popup: 'animated zoomIn'
        },
        buttonsStyling: false
        }).then((result) => {
            if (result.value){
                $("#"+form).removeClass('enviarForm');
                $("#"+idboton).attr("disabled",true);
                $("#"+form).trigger("submit");
            }
        });
}