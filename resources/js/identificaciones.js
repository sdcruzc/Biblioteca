$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmidentificacion', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var identificacion= $(this).attr('data-identificacion');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar la identificación "+identificacion+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#identificacion').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir identificacion');
        $('#frmidentificacion').attr('action',window.origin+'/save-identificacion');
    }
    else{
        $('.modal-title').html('Modificar identificacion');
        var id= $(btn).attr('data-id');
        var identificacion= $(btn).attr('data-identificacion');
        $('#identificacion').val(identificacion);
        $('#ididentificacion').val(id);
        $('#frmidentificacion').attr('action',window.origin+'/update-identificacion');
    }
    window.setTimeout(function() {
        $("#identificacion").trigger("focus");
    }, 500);
}