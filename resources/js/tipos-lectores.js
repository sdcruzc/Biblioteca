$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmtipos', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var tipos= $(this).attr('data-tipos');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar el tipo "+tipos+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#tipo').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir tipo');
        $('#frmtipo').attr('action',window.origin+'/save-tipos-lectores');
    }
    else{
        $('.modal-title').html('Modificar tipos');
        var id= $(btn).attr('data-id');
        var tipos= $(btn).attr('data-tipos');
        $('#tipo').val(tipos);
        $('#idtipo').val(id);
        $('#frmtipo').attr('action',window.origin+'/update-tipos-lectores');
    }
    window.setTimeout(function() {
        $("#tipo").trigger("focus");
    }, 500);
}