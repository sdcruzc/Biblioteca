$(function(){
    var rol= $('#role').val();
    var sucursal= $('#suc').val();
    if(rol==="Bibliotecario"){
        $('#sucursal').parent().addClass('d-none');
    }
    
});
$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmejemplar', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var codigo= $(this).attr('data-codigo');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de dar de baja el ejemplar "+codigo+"?";
    var msj="El ejemplar tendrá el estatus de baja y no se podrá prestar.";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var rol= $('#role').val();
    var op= $(btn).attr('data-op');
    $('#codigo').prop('readonly',false);
    $('#codigo').val('');
    $('#estante').val('');
    $('#pasillo').val('');
    if(rol !=="Bibliotecario"){
        $('#sucursal').val('');
    }

    $('#estatus').val('');
    $('#idejemplar').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir ejemplar');
        $('#frmejemplar').attr('action',window.origin+'/save-ejemplar');
    }
    else{
        $('.modal-title').html('Modificar ejemplar');
        var id= $(btn).attr('data-id');
        var codigo= $(btn).attr('data-codigo');
        var estante= $(btn).attr('data-estante');
        var pasillo= $(btn).attr('data-pasillo');
        var sucursal= $(btn).attr('data-sucursal');
        var estatus= $(btn).attr('data-estatus');
        $('#codigo').prop('readonly',true);
        $('#codigo').val(codigo);
        $('#estante').val(estante);
        $('#pasillo').val(pasillo);
        $('#sucursal').val(sucursal);
        $('#estatus').val(estatus);
        $('#idejemplar').val(id);
        $('#frmejemplar').attr('action',window.origin+'/update-ejemplar');
    }
    window.setTimeout(function() {
        $("#codigo").trigger("focus");
    }, 500);
}