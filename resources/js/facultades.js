$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmfacultad', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var facultad= $(this).attr('data-facultad');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar la facultad "+facultad+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#facultad').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir facultad');
        $('#frmfacultad').attr('action',window.origin+'/save-facultad');
    }
    else{
        $('.modal-title').html('Modificar facultad');
        var id= $(btn).attr('data-id');
        var facultad= $(btn).attr('data-facultad');
        $('#facultad').val(facultad);
        $('#idfacultad').val(id);
        $('#frmfacultad').attr('action',window.origin+'/update-facultad');
    }
    window.setTimeout(function() {
        $("#facultad").trigger("focus");
    }, 500);
}