$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('click', '.openModalPass', function(){
    modaPass(this);
});

$(document).on('submit', '#frmusuario', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var usuario= $(this).attr('data-usuario');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar al usuario "+usuario+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#nombre').val('');
    $('#email').val('');
    $('#pass').val('');
    $('#sucursal').val('');
    $('#rol').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir usuario');
        $('#frmusuario').attr('action',window.origin+'/save-user');
        $('#divEmail , #divPass').removeClass('d-none');
        $("#email , #pass").prop("required",true);
    }
    else{
        $('#divEmail , #divPass').addClass('d-none');
        $("#email , #pass").removeAttr("required");
        $('.modal-title').html('Modificar usuario');
        var id= $(btn).attr('data-id');
        var nombre= $(btn).attr('data-usuario');
        var sucursal= $(btn).attr('data-sucursal');
        var rol= $(btn).attr('data-rol');
        $('#nombre').val(nombre);
        $('#sucursal').val(sucursal);
        $('#rol').val(rol);
        $('#idusuario').val(id);
        $('#frmusuario').attr('action',window.origin+'/update-user');
    }
    window.setTimeout(function() {
        $("#nombre").trigger("focus");
    }, 500);
}

function modaPass(btn){
    $('#pass1').val('');
    $('#pass2').val('');
    var id= $(btn).attr('data-id');
    $('#iduser').val(id);
    window.setTimeout(function() {
        $("#pass1").trigger("focus");
    }, 500);
}