$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmcampus', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var campus= $(this).attr('data-campus');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar el campus "+campus+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#campus').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir campus');
        $('#frmcampus').attr('action',window.origin+'/save-campus');
    }
    else{
        $('.modal-title').html('Modificar campus');
        var id= $(btn).attr('data-id');
        var campus= $(btn).attr('data-campus');
        $('#campus').val(campus);
        $('#idcampus').val(id);
        $('#frmcampus').attr('action',window.origin+'/update-campus');
    }
    window.setTimeout(function() {
        $("#campus").trigger("focus");
    }, 500);
}