window.$ = window.jQuery = require('jquery');

require('./bootstrap');
import Swal from 'sweetalert2/dist/sweetalert2';
window.Swal = Swal;

require('./footable.min.js');

require( 'pdfmake' );
require( 'datatables.net-buttons/js/buttons.html5.js' )();
require( 'datatables.net-buttons/js/buttons.print.js' )();


import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import buttons from 'datatables.net-buttons-bs5';

import dt from 'datatables.net-bs5';
import jsZip from 'jszip';
window.JSZip = jsZip;
import 'datatables.net-responsive-bs5';

require('./utils.js');