var num=1;
$(document).on('click', '#btnAdd', function(){
    num++;
    var siguiente= 'div_clon_'+num;
    clonacion('div_clon_1','clones',siguiente);

});

$(document).on('click', '#btnDel', function(){
    if(num===1){
        show_alerta('El libro debe contener por lo menos 1 autor','warning','');
    }
    else{
        $('#div_clon_'+num).remove();
        num--;
    }
});

function clonacion(origen,destino,id){
    $('#'+origen).clone().appendTo('#'+destino).prop('id',id);
    $('#'+id+'> div > select').prop('id','autor_'+num);
}