$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmlector', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var lector= $(this).attr('data-lector');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar el lector "+lector+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#idlector').val('');
    $('#matricula').val('');
    $('#nombre').val('');
    $('#apellidos').val('');
    $('#nacimiento').val('');
    $('#curp').val('');
    $('#celular').val('');
    $('#telefono').val('');
    $('#correo').val('');
    $('#direccion').val('');
    $('#campus').val('');
    $('#facultad').val('');
    $('#tipo').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir lector');
        $('#frmlector').attr('action',window.origin+'/save-lector');
    }
    else{
        $('.modal-title').html('Modificar lector');
        var id= $(btn).attr('data-id');
        var matricula= $(btn).attr('data-matricula');
        var nombre= $(btn).attr('data-nombre');
        var apellidos= $(btn).attr('data-apellidos');
        var nacimiento= $(btn).attr('data-nacimiento');
        var curp= $(btn).attr('data-curp');
        var celular= $(btn).attr('data-celular');
        var telefono= $(btn).attr('data-telefono');
        var correo= $(btn).attr('data-correo');
        var direccion= $(btn).attr('data-direccion');
        var campus= $(btn).attr('data-campusid');
        var facultad= $(btn).attr('data-facultadid');
        var tipo= $(btn).attr('data-tipoid');
        $('#idlector').val(id);
        $('#matricula').val(matricula);
        $('#nombre').val(nombre);
        $('#apellidos').val(apellidos);
        $('#nacimiento').val(nacimiento);
        $('#curp').val(curp);
        $('#celular').val(celular);
        $('#telefono').val(telefono);
        $('#correo').val(correo);
        $('#direccion').val(direccion);
        $('#campus').val(campus);
        $('#facultad').val(facultad);
        $('#tipo').val(tipo);
        $('#frmlector').attr('action',window.origin+'/update-lector');
    }
    window.setTimeout(function() {
        $("#matricula").trigger("focus");
    }, 500);
}