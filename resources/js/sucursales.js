$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmsucursal', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var sucursal= $(this).attr('data-sucursal');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar la sucursal "+sucursal+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#sucursal').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir sucursal');
        $('#frmsucursal').attr('action',window.origin+'/save-sucursal');
    }
    else{
        $('.modal-title').html('Modificar sucursal');
        var id= $(btn).attr('data-id');
        var sucursal= $(btn).attr('data-sucursal');
        $('#sucursal').val(sucursal);
        $('#idsucursal').val(id);
        $('#frmsucursal').attr('action',window.origin+'/update-sucursal');
    }
    window.setTimeout(function() {
        $("#sucursal").trigger("focus");
    }, 500);
}