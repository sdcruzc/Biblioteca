$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmestatus', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var estatus= $(this).attr('data-estatus');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar el estatus "+estatus+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#estatus').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir estatus');
        $('#frmestatus').attr('action',window.origin+'/save-estatus-ejemplares');
    }
    else{
        $('.modal-title').html('Modificar estatus');
        var id= $(btn).attr('data-id');
        var estatus= $(btn).attr('data-estatus');
        $('#estatus').val(estatus);
        $('#idestatus').val(id);
        $('#frmestatus').attr('action',window.origin+'/update-estatus-ejemplares');
    }
    window.setTimeout(function() {
        $("#estatus").trigger("focus");
    }, 500);
}