$(document).on('click', '.openModal', function(){
    openModal(this);
});

$(document).on('submit', '#frmeditorial', function(){
    $(this).prop('disabled', true);
});

$(document).on('click', '.delete', function(){
    var id= $(this).attr('data-id');
    var editorial= $(this).attr('data-editorial');
    var form= "frm_del_"+id;
    var idboton= "btnDel_"+id;
    var titulo="¿Seguro de eliminar la editorial "+editorial+"?";
    var msj="No se podrá dar marcha atrás";
    var btn="<i class='fa-solid fa-check'></i> Eliminar";
    confirmacion(form,titulo,msj,btn,idboton);
});

function openModal(btn){
    var op= $(btn).attr('data-op');
    $('#editorial').val('');
    if(op==='1'){
        $('.modal-title').html('A&ntilde;adir editorial');
        $('#frmeditorial').attr('action',window.origin+'/save-editorial');
    }
    else{
        $('.modal-title').html('Modificar editorial');
        var id= $(btn).attr('data-id');
        var editorial= $(btn).attr('data-editorial');
        $('#editorial').val(editorial);
        $('#ideditorial').val(id);
        $('#frmeditorial').attr('action',window.origin+'/update-editorial');
    }
    window.setTimeout(function() {
        $("#editorial").trigger("focus");
    }, 500);
}