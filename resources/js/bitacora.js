$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on('click', '#btnBuscar', function(){
    var usuario = $('#usuario').val();
    var inicio = $('#f_ini').val(); 
    var fin = $('#f_fin').val()
    if((inicio == ''  && fin != '') || (inicio != ''  && fin == '')){
        show_alerta('Completa el rango de fechas','warning','');
    }
    else{
        $('#tbody').empty();
        $('#btnBuscar').prop('disabled', true);
        $('.dropdown-divider').removeClass('d-none');
        $('#reporte').addClass('d-none');
        $('#load').removeClass('d-none');
        var info = {usuario:usuario,inicio:inicio, fin:fin};
        tabla(info);
    }
});

let table;

function tabla(parametros){
    table= $('#tabla').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Bitácora',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Bitácora',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Bitácora',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-bitacora', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'name' }, //Estas son las colunas del json
            { data: 'accion' },
            { data: 'fecha2' },
            { data: 'hora' }
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}