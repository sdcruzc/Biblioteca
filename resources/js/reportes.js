$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on('click', '#btnBuscar', function(){
    var reporte= $('#reporte').val();
    if(reporte===''){
        show_alerta('Selecciona un reporte','warning','reporte');
    }
    else{
        $('.resultados').addClass('d-none');
        $('.dropdown-divider').removeClass('d-none');
        $(this).prop('disabled', true);
        $('#tbody_'+reporte).empty();
        $('#load').removeClass('d-none');
        var comparar = parseInt(jQuery.inArray(reporte, ['1','2','3','4','5']));
        console.log(comparar);
        if(comparar < 0){
            var info = {op:reporte,inicio:$('#f_ini').val(),fin:$('#f_fin').val()};
        }
        else{
            var info = {op:reporte};
        }

        if(reporte==='1'){
            tabla1(info);
        }
        else if(reporte==='2'){
            tabla2(info);
        }
        else if(reporte==='3'){
            tabla3(info);
        }
        else if(reporte==='4'){
            tabla4(info);
        }
        else if(reporte==='5'){
            tabla5(info);
        }
        else if(reporte==='6'){
            tabla6(info);
        }
    }
    
});

$(document).on('change','#reporte',function(){
    var op= $('#reporte').val();
    if(op ==='6'){
        $('.ocultar').removeClass('d-none');
    }
    else{
        $('.ocultar').addClass('d-none');
    }
});



let table;

function tabla1(parametros){
    $('#reporte_1').addClass('d-none');
    $('#load').removeClass('d-none');
    table= $('#tabla_1').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Inventario de ejemplares',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Inventario de ejemplares',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Inventario de ejemplares',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-reporte', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'isbn' }, //Estas son las colunas del json
            { data: 'titulo' },
            { data: 'codigo' },
            { data: 'estatus' },
            { data: 'pasillo' },
            { data: 'estante' }
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte_1').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}

function tabla2(parametros){
    $('#reporte_2').addClass('d-none');
    $('#load').removeClass('d-none');
    table= $('#tabla_2').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Libros más prestados',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Libros más prestados',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Libros más prestados',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-reporte', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'isbn' }, //Estas son las colunas del json
            { data: 'titulo' },
            { data: 'total' }
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte_2').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}


function tabla3(parametros){
    $('#reporte_3').addClass('d-none');
    $('#load').removeClass('d-none');
    table= $('#tabla_3').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Libros que no se han prestado',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Libros que no se han prestado',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Libros que no se han prestado',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-reporte', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'isbn' }, //Estas son las colunas del json
            { data: 'titulo' }
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte_3').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}

function tabla4(parametros){
    $('#reporte_4').addClass('d-none');
    $('#load').removeClass('d-none');
    table= $('#tabla_4').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Ejemplares en préstmo',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Ejemplares en préstmo',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Ejemplares en préstmo',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-reporte', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'matricula' }, //Estas son las colunas del json
            { data:null, render : function(data, type, row, meta) {
                return `${row.nombre + ' '+row.apellidos}`}},
            {data: 'titulo'},
            {data: 'codigo'},
            {data: 'identificacion'},
            {data: 'fecha_prestamo2'},
            {data: 'fecha_devolucion2'},
            { data:null, render : function(data, type, row, meta) {
                return (row.diferencia_dias > 0) ? row.diferencia_dias : '0'}},
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte_4').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}

function tabla5(parametros){
    $('#reporte_5').addClass('d-none');
    $('#load').removeClass('d-none');
    table= $('#tabla_5').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Lectores que han sobrepasado su fecha de entrega',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Lectores que han sobrepasado su fecha de entrega',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Lectores que han sobrepasado su fecha de entrega',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-reporte', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'matricula' }, //Estas son las colunas del json
            { data:null, render : function(data, type, row, meta) {
                return `${row.nombre + ' '+row.apellidos}`}},
            {data: 'titulo'},
            {data: 'fecha_prestamo2'},
            {data: 'fecha_devolucion2'},
            {data: 'fecha_devolucion_real2'},
            { data:'diferencia_entrega'}
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte_5').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}

function tabla6(parametros){
    $('#reporte_6').addClass('d-none');
    $('#load').removeClass('d-none');
    table= $('#tabla_6').DataTable({
        //retrieve: true,
        autoWidth: false,
        paging: true,
        ordering: false,
        info: true,
        responsive: 'true',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        dom: 'Bfrtip',

        buttons:[
            {
                title: 'Historial de préstmos',
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel"></i> Excel',
                className:'btn btn-success mb-3',
                excelStyles:{

                }
            },
        {
            extend:'pdfHtml5',
            title:'Historial de préstmos',
            text:'<i class="fa fa-file-pdf"></i> PDF',
            className:'btn btn-danger mb-3',
        },
        {
            extend:'print',
            title:'Historial de préstmos',
            text:'<i class="fa fa-print"></i> Imprimir',
            className:'btn btn-dark mb-3'
        }
    ],

        destroy: true,
        'ajax': {
            'url': 'show-reporte', //web a la que llamo y hace el trabajo.
             'type': 'POST', //Metodo usado por la funcion para pasar variable
            'dataSrc': '',
            'data': parametros, //En el data meto la variable cif con el contenido de dni, es formato json
         
        },
        columns: [
            {data:null, render : function(data, type, row, meta) {
                return `${meta.row + 1}`
            }},
            { data: 'matricula' }, //Estas son las colunas del json
            { data:null, render : function(data, type, row, meta) {
                return `${row.nombre + ' '+row.apellidos}`}},
            {data: 'titulo'},
            {data: 'codigo'},
            {data: 'identificacion'},
            {data: 'fecha_prestamo2'},
            {data: 'fecha_devolucion2'},
            { data:null, render : function(data, type, row, meta) {
                return (row.fecha_devolucion_real ===null) ? '' : row.fecha_devolucion_real2 }},
            { data:null, render : function(data, type, row, meta) {
                var atraso = (row.fecha_devolucion_real ===null) ? row.diferencia_dias : row.diferencia_entrega;
                return (atraso >0) ? atraso :'0' }},
           ],
        language: {
        search: 'Buscar:',
        zeroRecords: 'No hay registros para mostrar.',
        emptyTable: 'La tabla está vacia.',
        info: "Mostrando del _START_ a _END_ de _TOTAL_ Registros.",
        infoFiltered: "(Filtrados de _MAX_ Registros.)",
        paginate: {
            first: 'Primero',
            previous: 'Anterior',
            next: 'Siguiente',
            last: 'Último'
        }
        }
        
    });
    $('#load').addClass('d-none');
    $('#reporte_6').removeClass('d-none');
    $('#btnBuscar').prop('disabled', false);
}