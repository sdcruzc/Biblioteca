@extends('layouts.app')

@section('page-title')
Registrar libros
@endsection

@section('style')
    <link href="{{ asset('css/libros.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-warning shadow">
                <div class="card-header bg-warning"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        <form id="frmlibro" method="POST" action="{{url('save-libro')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" ><i class="fa-solid fa-barcode"></i></span>
                                        <input id="isbn" type="text" class="form-control" name="isbn" placeholder="ISBN" maxlength="50" autofocus required>
                                        <input id="idlibro" type="hidden" name="idlibro">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-book"></i></span>
                                        <input id="titulo" type="text" class="form-control" name="titulo" placeholder="T&iacute;tulo" maxlength="100" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-pencil"></i></span>
                                        <input id="subtitulo" type="text" class="form-control" name="subtitulo" placeholder="Subt&iacute;tulo" maxlength="100">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-comment"></i></span>
                                        <input id="descripcion" type="text" class="form-control" name="descripcion" placeholder="Descripci&oacute;n" maxlength="240">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-hashtag"></i></span>
                                        <input id="paginas" type="number" min="0" class="form-control" name="paginas" placeholder="N&uacute;mero de p&aacute;ginas">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-calendar"></i></span>
                                        <input id="publicacion" type="number" class="form-control" name="publicacion" placeholder="A&ntilde;o de publicaci&oacute;n" maxlength="4">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-book-open"></i></span>
                                        <select id="editorial" name="editorial" class="form-select" required>
                                            <option value="">Selecciona editorial</option>
                                            @foreach($editoriales as $row)
                                            <option value="{{$row->id}}">{{$row->editorial}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-bookmark"></i></span>
                                        <select id="categoria" name="categoria" class="form-select" data-live-search="true" required>
                                            <option value="">Selecciona categoía</option>
                                            @foreach($categorias as $row)
                                            <option value="{{$row->id}}">{{$row->categoria}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr class="dropdown-divider">
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <label class="h3">Autores</label>
                                    <button id="btnAdd" class="btn btn-success rounded-circle ms-3 me-3" type="button"><i class="fa-solid fa-plus"></i></button>
                                    <button id="btnDel" class="btn btn-danger rounded-circle" type="button"><i class="fa-solid fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="row mb-3" id="clones">
                                <div class="col-md-6" id="div_clon_1">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text"><i class="fa-solid fa-user-tie"></i></span>
                                        <select id="autor_1" name="autores[]" class="form-select losautores" required>
                                            <option value="">Selecciona autor</option>
                                            @foreach($autores as $row)
                                            <option value="{{$row->id}}">{{$row->nombre." ".$row->apellidos}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr class="dropdown-divider">
                            <div class="row">
                                <div class="col-md-6 offset-md-3">
                                    <div class="d-grid col-6 mx-auto">
                                        <button id="btnSave" class="btn btn-theme" type="subtmit"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/libros.js') }}"></script>
@endsection