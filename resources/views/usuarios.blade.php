@extends('layouts.app')

@section('page-title')
Usuarios
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-warning shadow">
                <div class="card-header bg-warning"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                                  @if(session()->get('warning'))
                                    <div class="alert alert-warning alert-fade">
                                        <i class="fa-solid fa-triangle-exclamation"></i>
                                        {{ session()->get("warning") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4 offset-md-4">
                                <div class="d-grid col-6 mx-auto">
                                    <button class="btn btn-theme openModal" type="button" data-bs-toggle="modal" data-bs-target="#modalusuario" data-op="1"><i class="fa-solid fa-circle-plus"></i> A&ntilde;adir</button>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered footable-p-f">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">NOMBRE</th>
                                                <th data-breakpoints="xs" scope="col">CORREO</th>
                                                <th data-breakpoints="xs" scope="col">ROL</th>
                                                <th data-breakpoints="xs sm md" scope="col">SUCURSAL</th>
                                                <th data-breakpoints="xs sm" scope="col">EDITAR</th>
                                                <th data-breakpoints="xs sm" scope="col">ELIMINAR</th>
                                                <th data-breakpoints="xs sm" scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($user as $row)
                                                <tr>
                                                    <td scope="row">{{$i++}}</td>
                                                    <td>{{$row->name}}</td>
                                                    <td>{{$row->email}}</td>
                                                    <td>{{$row->rol}}</td>
                                                    <td>{{$row->sucursal}}</td>
                                                    <td class="text-center"><button class="btn btn-warning openModal" data-bs-toggle="modal" data-bs-target="#modalusuario" data-op="2" data-id="{{$row->id}}" data-usuario="{{$row->name}}" data-sucursal="{{$row->id_sucursal}}" data-rol="{{$row->rol}}"><i class="fa-solid fa-pen-to-square"></i></button></td>
                                                    <td class="text-center">
                                                        <form id="frm_del_{{$row->id}}" method="POST" action="delete-user">
                                                            @csrf
                                                            <input id="idUs" type="hidden" name="idUs" value="{{$row->id}}">
                                                            <button id="btnDel_{{$row->id}}" class="btn btn-danger delete" data-usuario="{{$row->name}}" data-id="{{$row->id}}" type="button"><i class="fa-solid fa-trash-can"></i></button>
                                                        </form>
                                                    </td>
                                                    <td class="text-center"><button class="btn btn-dark openModalPass" data-bs-toggle="modal" data-bs-target="#modalpass" data-id="{{$row->id}}"><i class="fa-solid fa-lock"></i></button></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalusuario" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">A&ntilde;adir usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmusuario" method="POST" action="{{url('save-user')}}">
                    @csrf
                    <div class="row">
                        <input id="idusuario" type="hidden" name="idusuario">
                        <div class="col-md-12">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-user"></i></span>
                                <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre" maxlength="80" required>
                            </div>
                        </div>
                        <div class="col-md-12" id="divEmail">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-at"></i></span>
                                <input id="email" type="text" class="form-control" name="email" placeholder="Correo" maxlength="80" required>
                            </div>
                        </div>
                        <div class="col-md-12" id="divPass">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-lock"></i></span>
                                <input id="pass" type="password" class="form-control" name="pass" placeholder="Contrase&ntilde;a" maxlength="20" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                                <select id="sucursal" name="sucursal" class="form-select" data-live-search="true" required>
                                    <option value="">Selecciona sucursal</option>
                                    @foreach($sucursales as $row)
                                    <option value="{{$row->id}}">{{$row->sucursal}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-key"></i></span>
                                <select id="rol" name="rol" class="form-select" data-live-search="true" required>
                                    <option value="">Selecciona Rol</option>
                                    @foreach($roles as $row)
                                    <option value="{{$row->name}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-2">
                            <div class="d-grid col-6 mx-auto">
                                <button id="btnSave" class="btn btn-theme" type="subtmit"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
    </div>
    <div id="modalpass" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Restablecer contrase&ntilde;a</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{url('change-password')}}">
                    @csrf
                    <div class="row text-center fs-3">
                        <div class="col-12">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-lock"></i></span>
                                <input id="pass1" type="password" class="form-control" name="pass1" placeholder="Nueva contrase&ntilde;a" maxlength="20" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-lock"></i></span>
                                <input id="pass2" type="password" class="form-control" name="pass2" placeholder="Repetir contrase&ntilde;a" maxlength="20" required>
                            </div>
                        </div>
                        <input type="hidden" id="iduser" name="iduser">
                        <div class="col-sm-8 offset-sm-2 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                            <div class="d-grid">
                                <button class="btn btn-theme" type="submit"><i class="fa-solid fa-rotate"></i> Cambiar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
    </div>
    
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/usuarios.js') }}"></script>
@endsection