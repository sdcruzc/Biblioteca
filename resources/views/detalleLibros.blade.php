@extends('layouts.app')

@section('page-title')
Detalle de libro
@endsection

@section('style')
    <link href="{{ asset('css/libros.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-warning shadow">
                <div class="card-header bg-warning"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        @if(count($libro)>0)
                        <div class="row mb-3">
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-9">
                                <p class="h2 text-theme">{{$libro[0]->titulo}}</p>
                                <p class="h4">{{$libro[0]->subtitulo}}</p>
                                <div class="row">
                                    <div class="col-md-7">
                                        Autor: <label class="text-decoration-underline">{{$libro[0]->autores}}</label>
                                    </div>
                                    <div class="col-md-5">
                                        Editorial: <b>{{$libro[0]->editorial}}</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        ISBN: <b>{{$libro[0]->isbn}}</b>
                                    </div>
                                    <div class="col-md-5">
                                        Páginas: <b>{{$libro[0]->paginas}}</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Categoría: <b>{{$libro[0]->categoria}}</b>
                                    </div>
                                    <div class="col-md-5">
                                        Publicación: <b>{{$libro[0]->publicacion}}</b>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <p class="h4">Sinopsis</p>
                                    <div class="col-12 justify-content">
                                        {{$libro[0]->descripcion}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="dropdown-divider">
                        <div class="row mt-2">
                            <div class="col-12"><label class="h5">EJEMPLARES:</label></div>
                            <div class="col-md-4 offset-md-4">
                                <div class="d-grid col-6 mx-auto">
                                    <button class="btn btn-theme openModal" type="button" data-bs-toggle="modal" data-bs-target="#modalEjemplar" data-op="1"><i class="fa-solid fa-circle-plus"></i> A&ntilde;adir</button>
                                  </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">CODIGO</th>
                                                <th data-breakpoints="xs" scope="col">PASILLO</th>
                                                <th data-breakpoints="xs" scope="col">ESTANTE</th>
                                                <th data-breakpoints="xs sm md" scope="col">SUCURSAL</th>
                                                <th data-breakpoints="xs" scope="col">ESTATUS</th>
                                                <th data-breakpoints="xs" scope="col"></th>
                                                <th data-breakpoints="xs" scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($ejemplares as $row)
                                                <tr>
                                                    <td scope="row">{{$i++}}</td>
                                                    <td>{{$row->codigo}}</td>
                                                    <td>{{$row->pasillo}}</td>
                                                    <td>{{$row->estante}}</td>
                                                    <td>{{$row->sucursal}}</td>
                                                    <td>{{$row->estatus}}</td>
                                                    <td>
                                                        @if(Auth()->user()->roles[0]->name =="Bibliotecario" && $sucursal==$row->id_sucursal  && $row->id_estatus >1)
                                                            <button class="btn btn-warning openModal" data-op="2" type="button" data-bs-toggle="modal" data-bs-target="#modalEjemplar" data-id="{{$row->id}}"  data-codigo="{{$row->codigo}}" data-pasillo="{{$row->pasillo}}" data-estante="{{$row->estante}}" data-sucursal="{{$row->id_sucursal}}" data-estatus="{{$row->id_estatus}}"><i class="fa-solid fa-pen-to-square"></i></button>
                                                        @elseif(Auth()->user()->roles[0]->name !="Bibliotecario"  && $row->id_estatus >1)
                                                            <button class="btn btn-warning openModal" data-op="2" type="button" data-bs-toggle="modal" data-bs-target="#modalEjemplar" data-id="{{$row->id}}"  data-codigo="{{$row->codigo}}" data-pasillo="{{$row->pasillo}}" data-estante="{{$row->estante}}" data-sucursal="{{$row->id_sucursal}}" data-estatus="{{$row->id_estatus}}"><i class="fa-solid fa-pen-to-square"></i></button>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <form id="frm_del_{{$row->id}}" method="POST" action="{{url('delete-ejemplar')}}">
                                                            @csrf
                                                            <input type="hidden" name="libro" value="{{ $libro[0]->id }}">
                                                        @if(Auth()->user()->roles[0]->name =="Bibliotecario" && $sucursal==$row->id_sucursal && $row->id_estatus >2)
                                                            <input type="hidden" name="idEje" value="{{$row->id}}">
                                                            <button type="button" id="btnDel_{{$row->id}}" data-id="{{$row->id}}" data-codigo="{{$row->codigo}}" class="btn btn-danger delete"><i class="fa-solid fa-trash-can"></i></button>
                                                        @elseif(Auth()->user()->roles[0]->name !="Bibliotecario" && $row->id_estatus >2)
                                                            <input type="hidden" name="idEje" value="{{$row->id}}">
                                                            <button type="button" id="btnDel_{{$row->id}}" data-id="{{$row->id}}" data-codigo="{{$row->codigo}}" class="btn btn-danger delete"><i class="fa-solid fa-trash-can"></i></button>
                                                        @endif
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @else
                        No existe
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalEjemplar" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">A&ntilde;adir autor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmejemplar" method="POST" action="{{url('save-ejemplar')}}">
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-barcode"></i></span>
                        <input id="codigo" type="text" class="form-control" name="codigo" placeholder="Código" maxlength="30" required>
                        <input id="idejemplar" type="hidden" name="idejemplar">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-map"></i></span>
                        <input id="pasillo" type="text" class="form-control" name="pasillo" placeholder="Pasillo" maxlength="15" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-archway"></i></span>
                        <input id="estante" type="text" class="form-control" name="estante" placeholder="Estante" maxlength="15" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                        <select id="sucursal" name="sucursal" class="form-select" required>
                            @if(Auth()->user()->roles[0]->name =="Bibliotecario")
                                @foreach($sucursales as $row)
                                    @if(Auth()->user()->roles[0]->name =="Bibliotecario" && $sucursal==$row->id)
                                        <option value="{{$row->id}}" selected>{{$row->sucursal}}</option>
                                    @endif
                                @endforeach
                            @else
                                <option value="">Selecciona sucursal</option>
                                @foreach($sucursales as $row)
                                    <option value="{{$row->id}}">{{$row->sucursal}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-book-atlas"></i></span>
                        <select id="estatus" name="estatus" class="form-select" data-live-search="true" required>
                            <option value="">Selecciona estatus</option>
                            @foreach($estatus as $row)
                            <option value="{{$row->id}}">{{$row->estatus}}</option>
                            @endforeach
                        </select>
                    </div>
                    @if(count($libro)>0)
                        <input type="hidden" name="libro" value="{{ $libro[0]->id }}">
                    @endif
                    <div class="col-md-6 offset-md-2">
                        <div class="d-grid col-6 mx-auto">
                            <button id="btnSave" class="btn btn-theme" type="subtmit"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
    </div>
    <input type="hidden" id="role" value="{{ Auth()->user()->roles[0]->name }}">
    <input type="hidden" id="suc" value="{{ $sucursal }}">
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/detalle-libro.js') }}"></script>
@endsection