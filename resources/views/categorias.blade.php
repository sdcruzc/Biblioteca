@extends('layouts.app')

@section('page-title')
Categor&iacute;as
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-info shadow">
                <div class="card-header bg-info"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row  mb-3">
                                    <div class="col-md-10 offset-md-1">
                                        <div class="d-grid col-6 mx-auto">
                                            <button class="btn btn-theme openModal" type="button" data-bs-toggle="modal" data-bs-target="#modalCategorias" data-op="1"><i class="fa-solid fa-circle-plus"></i> A&ntilde;adir</button>
                                          </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered footable-p-f">
                                        <thead><tr><th scope="col">#</th><th scope="col">NOMBRE</th><th scope="col">EDITAR</th><th scope="col">ELIMINAR</th></tr></thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($categorias as $row)
                                                <tr><th scope="row">{{$i++}}</th><td>{{$row->categoria}}</td>
                                                    <td class="text-center"><button class="btn btn-warning openModal" data-bs-toggle="modal" data-bs-target="#modalCategorias" data-op="2" data-categoria="{{$row->categoria}}" data-id="{{$row->id}}"><i class="fa-solid fa-pen-to-square"></i></button></td>
                                                    <td class="text-center">
                                                        <form id="frm_del_{{$row->id}}" method="POST" action="delete-categoria">
                                                            @csrf
                                                            <input id="idCat" type="hidden" name="idCat" value="{{$row->id}}">
                                                            <button id="btnDel_{{$row->id}}" class="btn btn-danger delete" data-categoria="{{$row->categoria}}" data-id="{{$row->id}}" type="button"><i class="fa-solid fa-trash-can"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr class="dropdown-divider d-block d-md-none mb-4 mb-md-0">
                            <div class="col-md-6">
                                <div class="row mb-3">
                                    <div class="col-md-5 offset-md-3 text-center">
                                        <label class="h3">TOP 10 </label>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered footable-p-f">
                                        <thead><tr><th scope="col">#</th><th scope="col">CATEGORIA</th><th scope="col">TOTAL</th></thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($top as $row)
                                                <tr><th scope="row">{{$i++}}</th><td>{{$row->categoria}}</td><td>{{$row->total}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalCategorias" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">A&ntilde;adir categor&iacute;a</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmCategoria" method="POST" action="{{url('save-categoria')}}">
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-bookmark"></i></span>
                        <input id="categoria" type="text" class="form-control" name="categoria" placeholder="Categor&iacute;a" maxlength="50" required>
                        <input id="idCategoria" type="hidden" name="idCategoria">
                    </div>
                    <div class="col-md-6 offset-md-2">
                        <div class="d-grid col-6 mx-auto">
                            <button id="btnSave" class="btn btn-theme" type="subtmit"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/categorias.js') }}"></script>
@endsection