@extends('layouts.app')

@section('page-title')
Préstamo de libros
@endsection

@section('content')
<div class="row animate__animated animate__zoomInDown">
    <div class="col-md-12">
        <div class="card border-warning shadow">
            <div class="card-header bg-warning"></div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-md-12">
                            @if(session()->get('success'))
                                <div class="alert alert-success alert-fade">
                                    <i class="fa-solid fa-check"></i>
                                    {{ session()->get("success") }}
                                </div>
                              @endif
                              @if(session()->get('error'))
                                <div class="alert alert-danger alert-fade">
                                    <i class="fa-solid fa-xmark"></i>
                                    {{ session()->get("error") }}
                                </div>
                              @endif
                        </div>
                    </div>
                    <div class="row">
                        @if($sucursal==0)
                            <div class="col-md-12 mb-3">
                                <div class="alert alert-danger"><i class="fa-solid fa-xmark"></i>  NO PUEDES REALIZAR PRESTAMOS DEBIDO A QUE NO TIENES SUCURSAL ASIGNADA, CONTACTA AL ADMINSITRADOR DEL SISTEMA.</div>
                            </div>
                        @else
                            <div class="col-md-6 offset-md-3 mb-3">
                                <div class="d-grid col-6 mx-auto">
                                    <button class="btn btn-theme openModalPrestamo" type="button" data-bs-toggle="modal" data-bs-target="#modalprestar" data-op="1"><i class="fa-solid fa-handshake"></i>  Préstamo</button>
                                </div>
                            </div>
                            <hr class="dropdown-divider mt-3">
                            <div class="col-md-12 h4 text-center mt-2">Libros que están en préstamo</div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped footable-p-f">
                                        <thead><tr><th data-breakpoints="xs">#</th><th>MATRICULA</th><th>LECTOR</th><th>LIBRO</th><th>CODIGO</th><th data-breakpoints="xs sm">IDENTIFICACION</th><th>FECHA PRESTAMO</th><th data-breakpoints="xs">FECHA DEVOLUCION</th><th data-breakpoints="xs sm">DIAS ATRASO</th><th data-breakpoints="xs sm"></th></tr></thead>
                                        <tbody>
                                            @php
                                            $i=1;
                                            @endphp
                                            @foreach($prestamos as $row)
                                            <tr><td>{{$i++}}</td><td>{{$row->matricula}}</td><td>{{$row->nombre." ".$row->apellidos}}</td><td>{{$row->titulo}}</td><td>{{$row->codigo}}</td><td>{{$row->identificacion}}</td><td>{{$row->fecha_prestamo2}}</td><td>{{$row->fecha_devolucion2}}</td><td>@if($row->diferencia_dias >0 ) <div class="text-danger">{{$row->diferencia_dias}}</div>  @endif</td><td><button class="btn btn-theme btnDevolver" data-id="{{$row->id}}" data-codigo="{{$row->codigo}}" data-titulo="{{$row->titulo}}" data-nombre="{{$row->nombre.' '.$row->apellidos}}" type="button"><i class="fa-solid fa-hands-holding"></i>  Devolver</button></td></tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modalprestar" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Préstamo de libros</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="frmautor" method="POST" action="{{url('save-prestamo')}}">
                @csrf
                <input type="hidden" id="idLector" name="idLector"><input type="hidden" id="idEjemplar" name="idEjemplar">
                <div class="row">
                    <div class="col-md-4">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-id-badge"></i></span>
                            <input id="matricula" type="text" class="form-control" name="matricula" placeholder="Matrícula" maxlength="30" data-bs-toggle="tooltip" title="Escribe la matrícula y presiona enter" required>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-user"></i></span>
                            <label id="lblLector" class="form-control">&nbsp;</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-barcode"></i></span>
                            <input id="codigo" type="text" class="form-control" name="codigo" placeholder="Ejemplar" maxlength="30" data-bs-toggle="tooltip" title="Escribe el código y presiona enter" required>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-book"></i></span>
                            <label id="lblLibro" class="form-control">&nbsp;</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-calendar-alt"></i></span>
                            <input id="fecha" name="fecha" type="date" class="form-control" data-bs-toggle="tooltip" title="Fecha de devolución" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-address-card"></i></span>
                            <select id="identificacion" name="identificacion" class="form-select" required>
                                <option value="">Selecciona identificacion</option>
                                @foreach($identificaciones as $row)
                                <option value="{{$row->id}}">{{$row->identificacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="divMsj" class="col-md-12 d-none">
                        <div id="msj" class="alert alert-danger"></div>
                    </div>
                    <div id="divBtn" class="col-md-12 d-none">
                        <div class="d-grid col-6 mx-auto">
                            <button id="btnSavePrestamo" class="btn btn-theme" type="button"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
</div>
<form id="frm_devolucion" method="POST" action="{{url('save-devolucion')}}">
    @csrf
    <input type="hidden" id="dev_id" name="dev_id">
    <input type="hidden" id="dev_suc" name="dev_suc" value="{{$sucursal}}">
</form>
<input type="hidden" id="sucursal" name="sucursal" value="{{$sucursal}}">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/prestamos.js') }}"></script>
@endsection