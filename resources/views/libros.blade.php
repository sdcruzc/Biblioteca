@extends('layouts.app')

@section('page-title')
Libros
@endsection

@section('style')
    <link href="{{ asset('css/libros.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-warning shadow">
                <div class="card-header bg-warning"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4 offset-md-4">
                                <div class="d-grid col-6 mx-auto">
                                    <a class="btn btn-theme" href="{{url('crear-libros')}}" ><i class="fa-solid fa-circle-plus"></i> A&ntilde;adir</a>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered footable-p-f">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th data-breakpoints="xs"  scope="col">ISBN</th>
                                                <th scope="col">TITULO</th>
                                                <th data-breakpoints="xs" scope="col">SUBTITULO</th>
                                                <th data-breakpoints="xs" scope="col">AUTORES</th>
                                                <th data-breakpoints="xs" scope="col">P&Aacute;GINAS</th>
                                                <th data-breakpoints="xs" scope="col">A&Ntilde;O</th>
                                                <th data-breakpoints="xs" scope="col">EDITORIAL</th>
                                                <th data-breakpoints="xs sm" scope="col">CATEGORIA</th>
                                                <th data-breakpoints="xs sm" scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($libros as $row)
                                                <tr>
                                                    <td scope="row">{{$i++}}</td>
                                                    <td>{{$row->isbn}}</td>
                                                    <td>{{$row->titulo}}</td>
                                                    <td>{{$row->subtitulo}}</td>
                                                    <td>{{$row->autores}}</td>
                                                    <td>{{$row->paginas}}</td>
                                                    <td>{{$row->publicacion}}</td>
                                                    <td>{{$row->editorial}}</td>
                                                    <td>{{$row->categoria}}</td>
                                                    <td><a class="btn btn-info" href="{{ route('libros.show',$row->id) }}"><i class="fa-solid fa-circle-info"></i></a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
