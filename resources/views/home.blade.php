@extends('layouts.app')

@section('page-title')
Bienvenido!
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-theme shadow">
                <div class="card-header bg-theme"></div>
                    <div class="card-body">
                        <nav>
                            <div class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true"><span class="d-block d-sm-none"><span class="hidden-sm-up"><i class="fa-solid fa-home"></i></span></span><span class="d-none d-sm-block"> Tablero</span></button>
                                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false"><span class="d-block d-sm-none"><span class="hidden-sm-up"><i class="fa-solid fa-circle-user"></i></span></span><span class="d-none d-sm-block"> Mi cuenta</span></button>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row fs-3">
                                                    <div class="col">
                                                        <i class="fa-solid fa-handshake text-warning"></i>
                                                    </div>
                                                    <div class="col px-0">
                                                        <label class="text-info fs-5">Préstamos</label>
                                                    </div>
                                                    <div class="col">
                                                        <span class="badge rounded-pill bg-theme text-right text-warning">{{ $prestamos}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row fs-3">
                                                    <div class="col">
                                                        <i class="fa-solid fa-book text-warning"></i>
                                                    </div>
                                                    <div class="col px-0">
                                                        <label class="text-info fs-5">Ejemplares</label>
                                                    </div>
                                                    <div class="col">
                                                        <span class="badge rounded-pill bg-theme text-right text-warning">{{ $libros}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row fs-3">
                                                    <div class="col">
                                                        <i class="fa-solid fa-user-tie text-warning"></i>
                                                    </div>
                                                    <div class="col px-0">
                                                        <label class="text-info fs-5">Autores</label>
                                                    </div>
                                                    <div class="col">
                                                        <span class="badge rounded-pill bg-theme text-right text-warning">{{ $autores}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row fs-3">
                                                    <div class="col">
                                                        <i class="fa-solid fa-book-open text-warning"></i>
                                                    </div>
                                                    <div class="col px-0">
                                                        <label class="text-info fs-5">Editoriales</label>
                                                    </div>
                                                    <div class="col">
                                                        <span class="badge rounded-pill bg-theme text-right text-warning">{{ $editoriales}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row fs-3">
                                                    <div class="col">
                                                        <i class="fa-solid fa-bookmark text-warning"></i>
                                                    </div>
                                                    <div class="col px-0">
                                                        <label class="text-info fs-5">Categorías</label>
                                                    </div>
                                                    <div class="col">
                                                        <span class="badge rounded-pill bg-theme text-right text-warning">{{ $categorias}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row fs-3">
                                                    <div class="col">
                                                        <i class="fa-solid fa-book-open-reader text-warning"></i>
                                                    </div>
                                                    <div class="col px-0">
                                                        <label class="text-info fs-5">Lectores</label>
                                                    </div>
                                                    <div class="col">
                                                        <span class="badge rounded-pill bg-theme text-right text-warning">{{ $lectores}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="row">
                                    <div class="col-md-6 offset-md-3">
                                        @if(session()->get('success'))
                                            <div class="alert alert-success alert-fade">
                                                <i class="fa-solid fa-check"></i>
                                                {{ session()->get("success") }}
                                                <label class="res d-none">1</label>
                                            </div>
                                          @endif
                                          @if(session()->get('error'))
                                            <div class="alert alert-danger alert-fade">
                                                <i class="fa-solid fa-xmark"></i>
                                                {{ session()->get("error") }}
                                                <label class="res d-none">1</label>
                                            </div>
                                          @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row text-center fs-3">
                                                    <div class="col-1 col-sm-2">
                                                        <i class="fa-solid fa-user text-warning"></i>
                                                    </div>
                                                    <div class="col-11 col-sm-10 px-0">
                                                        <label class="text-theme fs-5">{{ Auth::user()->name }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row text-center fs-3">
                                                    <div class="col-1 col-sm-2">
                                                        <i class="fa-solid fa-envelope text-warning"></i>
                                                    </div>
                                                    <div class="col-11 col-sm-10 px-0">
                                                        <label class="text-theme fs-5">{{ Auth::user()->email }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row text-center fs-3">
                                                    <div class="col-1 col-sm-2">
                                                        <i class="fa-solid fa-key text-warning"></i>
                                                    </div>
                                                    <div class="col-11 col-sm-10 px-0">
                                                        <label class="text-theme fs-5">{{ Auth()->user()->roles[0]->name }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-3">
                                        <div class="card card-hover">
                                            <div class="card-body">
                                                <div class="row text-center fs-3">
                                                    <div class="col-1 col-sm-2">
                                                        <i class="fa-solid fa-building text-warning"></i>
                                                    </div>
                                                    <div class="col-11 col-sm-10 px-0">
                                                        <label class="text-theme fs-5">{{ $sucursal }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-3">
                                        <div class="card">
                                            <div class="card-body">
                                                <form method="POST" action="{{url('change-password')}}">
                                                    @csrf
                                                    <div class="row text-center fs-3">
                                                        <div class="col-12 mb-3">
                                                            Cambiar contrase&ntilde;a
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-lock"></i></span>
                                                                <input id="pass1" type="password" class="form-control" name="pass1" placeholder="Nueva contrase&ntilde;a" maxlength="20" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="input-group mb-3">
                                                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-lock"></i></span>
                                                                <input id="pass2" type="password" class="form-control" name="pass2" placeholder="Repetir contrase&ntilde;a" maxlength="20" required>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="iduser" value="{{ Auth::user()->id }}">
                                                        <div class="col-sm-8 offset-sm-2 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                                                            <div class="d-grid">
                                                                <button class="btn btn-theme" type="submit"><i class="fa-solid fa-rotate"></i> Cambiar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-3">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row text-center fs-3">
                                                    <div class="col-12 mt-2">
                                                        Miembro desde:
                                                    </div>
                                                    <div class="col-12 text-theme">
                                                        {{$fecha}}
                                                    </div>
                                                    <div class="col-12">
                                                        &Uacute;ltimo acceso:
                                                    </div>
                                                    <div class="col-12 text-theme fs-4">
                                                        {{$acceso_fecha}}
                                                    </div>
                                                    <div class="col-12 text-theme fs-4">
                                                         a las {{$acceso_hora}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/home.js') }}"></script>
@endsection