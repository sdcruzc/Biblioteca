<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Sistema de bilbioteca para universidades">
  <meta name="author" content="Daniel Cruz Alejandro">
  <meta property="og:locale" content="es_MX" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Biblioteca</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  @yield('style')
</head>
<body>
  <header class="navbar navbar-dark sticky-top bg-theme flex-lg-nowrap p-0">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ url('/') }}">Biblioteca</a>
    <button class="navbar-toggler position-absolute d-lg-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </header>
  <div class="container-fluid">
    @guest
        @yield('content')
    @else
    <div class="row">
      <nav id="sidebarMenu" class="col-md-4 col-lg-2 d-lg-block bg-white text-dark sidebar collapse bg-striped">
        <div class="position-sticky">
          <ul class="nav flex-column">
            <li class="nav-item mb-0">
              <div class="text-center"><label class="fw-bold">{{ Auth::user()->name }}</label>
                <p class="fw-light mb-0">{{ Auth()->user()->roles[0]->name }}</p>
              </div>
            </li>
            <li class="border-top my-1"></li>
            @if(Auth()->user()->roles[0]->id <3)
            <li class="nav-item">
              <a class="nav-link @if(Request::path() == 'usuarios') active @endif " href="{{ url('usuarios') }}" aria-current="page">
                <i class="fa-solid fa-user"></i>
                Usuarios
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if(Request::path() == 'bitacora') active @endif " href="{{ url('bitacora') }}" aria-current="page">
                <i class="fa-solid fa-user-secret"></i>
                Bitácora
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link @if(Request::path() == 'libros') active @endif " href="{{ url('libros') }}" aria-current="page">
                <i class="fa-solid fa-book"></i>
                Libros
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if(Request::path() == 'prestamos') active @endif " href="{{ url('prestamos') }}">
                <i class="fa-solid fa-handshake"></i>
                Préstamos
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link  @if(Request::path() == 'lectores') active @endif " href="{{ url('lectores') }}">
                <i class="fa-solid fa-book-open-reader"></i>
                Lectores
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link  @if(Request::path() == 'reportes') active @endif " href="{{ url('reportes') }}">
                <i class="fa-solid fa-file-lines"></i>
                Reportes
              </a>
            </li>
            <li class="mb-1">
              <button id="btn-toggle-catalogos" class="btn btn-toggle align-items-center" data-bs-toggle="collapse" data-bs-target="#orders-collapse" aria-expanded="false">
                Cat&aacute;logos
              </button>
              <div class="collapse" id="orders-collapse">
                <ul class=" nav flex-column ">
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'autores') active @endif " href="{{ url('autores') }}" ><i class="fa-solid fa-user-tie"></i> Autores</a></li>
                  @if(Auth()->user()->roles[0]->id <3)
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'campus') active @endif " href="{{ url('campus') }}" ><i class="fa-solid fa-school"></i> Campus</a></li>
                  @endif
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'categorias') active @endif " href="{{ url('categorias') }}" ><i class="fa-solid fa-bookmark"></i> Categor&iacute;as</a></li>
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'editoriales') active @endif " href="{{ url('editoriales') }}" class="link-dark"><i class="fa-solid fa-book-open"></i> Editoriales</a></li>
                  @if(Auth()->user()->roles[0]->id <3)
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'estatus-ejemplares') active @endif " href="{{ url('estatus-ejemplares') }}" ><i class="fa-solid fa-book-atlas"></i> Estatus de ejemplares</a></li>
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'facultades') active @endif " href="{{ url('facultades') }}"><i class="fa-solid fa-graduation-cap"></i> Facultades</a></li>
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'identificaciones') active @endif " href="{{ url('identificaciones') }}"><i class="fa-solid fa-address-card"></i> Identificaciones</a></li>
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'tipos-lectores') active @endif " href="{{ url('tipos-lectores') }}"><i class="fa-solid fa-user-gear"></i> Tipos de lectores</a></li>
                  <li class="nav-item"><a class="nav-link ps-5 @if(Request::path() == 'sucursales') active @endif " href="{{ url('sucursales') }}"><i class="fa-solid fa-building"></i> Sucursales</a></li>
                  @endif
                </ul>
              </div>
            </li>
            <li class="border-top my-3"></li>
            <li class="nav-item">
              <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                <button type="submit" class="btn-nav-link">
                <i class="fa-solid fa-power-off"></i> Salir
                </button>
              @csrf
              </form>
              </li>
          </ul>
        </div>
      </nav>
      <main class="col-lg-9 ms-sm-auto col-lg-10 px-md-3">
        <div class="page-titles bg-striped bg-white d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-2 pb-1 mb-3 border-bottom">
          <p class="h3">@yield('page-title')</p>
        </div>
        @yield('content')
      </main>
    </div>
    @endguest
  </div>
  </body>
  <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
  @yield('scripts')
</html>