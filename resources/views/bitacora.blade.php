@extends('layouts.app')

@section('page-title')
Bitácora
@endsection

@section('content')
<div class="row animate__animated animate__zoomInDown">
    <div class="col-md-12">
        <div class="card border-warning shadow">
            <div class="card-header bg-warning"></div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-md-4">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-user"></i></span>
                            <select id="usuario" class="form-select">
                                <option value="">Selecciona usuario</option>
                                @if($users)
                                    @foreach($users as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 ocultar">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-calendar-alt"></i></span>
                            <input type="date" class="form-control" id="f_ini" name="f_ini" data-bs-toggle="tooltip" title="Fecha inicio">
                        </div>
                    </div>
                    <div class="col-md-4 ocultar">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-calendar-alt"></i></span>
                            <input type="date" class="form-control" id="f_fin" name="f_fin" data-bs-toggle="tooltip" title="Fecha fin">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button id="btnBuscar" class="btn btn-theme"><i class="fa-solid fa-magnifying-glass"></i> Consultar</button>
                    </div>
                </div>
                <hr class="dropdown-divider mt-3 mb-3 d-none">
                <div class="row resultados d-none" id="reporte">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="tabla" class="table table-bordered">
                                <thead><tr><th>#</th><th>USUARIO</th><th>ACCIÓN</th><th>FECHA</th><th>HORA</th></tr></thead>
                                <tbody id="tbody"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row d-none" id="load">
                    <div class="col-md-4 offset-md-4">
                        <img src="{{ asset('images/loading_'.rand(1, 12).'.gif')}}" class="img-fluid" height="100%" width="100%"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/bitacora.js') }}"></script>
@endsection