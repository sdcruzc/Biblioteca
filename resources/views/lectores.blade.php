@extends('layouts.app')

@section('page-title')
Lectores
@endsection

@section('content')
    <div class="row animate__animated animate__zoomInDown" id="app">
        <div class="col-md-12">
            <div class="card border-warning shadow">
                <div class="card-header bg-warning"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4 offset-md-4">
                                <div class="d-grid col-6 mx-auto">
                                    <button class="btn btn-theme openModal" type="button" data-bs-toggle="modal" data-bs-target="#modallector" data-op="1"><i class="fa-solid fa-circle-plus"></i> A&ntilde;adir</button>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered footable-p-f">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th data-breakpoints="xs"  scope="col">MATRICULA</th>
                                                <th scope="col">NOMBRE</th>
                                                <th data-breakpoints="xs" scope="col">APELLIDOS</th>
                                                <th data-breakpoints="xs" scope="col">NACIMIENTO</th>
                                                <th data-breakpoints="xs sm md" scope="col">CURP</th>
                                                <th data-breakpoints="xs" scope="col">CELULAR</th>
                                                <th data-breakpoints="xs" scope="col">TELEFONO</th>
                                                <th data-breakpoints="xs" scope="col">CORREO</th>
                                                <th data-breakpoints="xs sm" scope="col">DIRECCION</th>
                                                <th data-breakpoints="xs sm" scope="col">CAMPUS</th>
                                                <th data-breakpoints="xs sm" scope="col">FACULTAD</th>
                                                <th data-breakpoints="xs sm" scope="col">TIPO</th>
                                                <th data-breakpoints="xs sm" scope="col">EDITAR</th>
                                                <th data-breakpoints="xs sm" scope="col">ELIMINAR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($lectores as $row)
                                                <tr>
                                                    <td scope="row">{{$i++}}</td>
                                                    <td>{{$row->matricula}}</td>
                                                    <td>{{$row->nombre}}</td>
                                                    <td>{{$row->apellidos}}</td>
                                                    <td>{{$row->nacimiento}}</td>
                                                    <td>{{$row->curp}}</td>
                                                    <td>{{$row->celular}}</td>
                                                    <td>{{$row->telefono}}</td>
                                                    <td>{{$row->correo}}</td>
                                                    <td>{{$row->direccion}}</td>
                                                    <td>{{$row->campus}}</td>
                                                    <td>{{$row->facultad}}</td>
                                                    <td>{{$row->tipo_lector}}</td>
                                                    <td class="text-center"><button class="btn btn-warning openModal" data-bs-toggle="modal" data-bs-target="#modallector" data-op="2" data-matricula="{{$row->matricula}}" data-nombre="{{$row->nombre}}" data-apellidos="{{$row->apellidos}}" data-nacimiento="{{$row->fecha_nacimiento}}" data-curp="{{$row->curp}}" data-celular="{{$row->celular}}" data-telefono="{{$row->telefono}}" data-correo="{{$row->correo}}" data-direccion="{{$row->direccion}}" data-campusid="{{$row->id_campus}}"  data-facultadid="{{$row->id_facultad}}" data-tipoid="{{$row->id_tipo_lector}}" data-id="{{$row->id}}"><i class="fa-solid fa-pen-to-square"></i></button></td>
                                                    <td class="text-center">
                                                        <form id="frm_del_{{$row->id}}" method="POST" action="delete-lector">
                                                            @csrf
                                                            <input id="idLec" type="hidden" name="idLec" value="{{$row->id}}">
                                                            <button id="btnDel_{{$row->id}}" class="btn btn-danger delete" data-lector="{{$row->nombre ." ".$row->apellidos}}"  data-id="{{$row->id}}" type="button"><i class="fa-solid fa-trash-can"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modallector" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">A&ntilde;adir lector</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmlector" method="POST" action="{{url('save-lector')}}">
                    @csrf
                    <div class="row">
                        <input id="idlector" type="hidden" name="idlector">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-id-badge"></i></span>
                                <input id="matricula" type="text" class="form-control" name="matricula" placeholder="Matricula" maxlength="20" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-user"></i></span>
                                <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre" maxlength="75" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-user"></i></span>
                                <input id="apellidos" type="text" class="form-control" name="apellidos" placeholder="Apellidos" maxlength="75" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-cake-candles"></i></span>
                                <input id="nacimiento" type="date" class="form-control" name="nacimiento" placeholder="Fecha de nacimiento">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-c"></i></span>
                                <input id="curp" type="text" class="form-control" name="curp" placeholder="Curp" maxlength="30">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-mobile-screen-button"></i></span>
                                <input id="celular" type="text" class="form-control" name="celular" placeholder="Celular" maxlength="10" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-phone"></i></span>
                                <input id="telefono" type="text" class="form-control" name="telefono" placeholder="Teléfono" maxlength="10">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-at"></i></span>
                                <input id="correo" type="email" class="form-control" name="correo" placeholder="Correo" maxlength="80" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-map"></i></span>
                                <input id="direccion" type="text" class="form-control" name="direccion" placeholder="Dirección" maxlength="200">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-school"></i></span>
                                <select id="campus" name="campus" class="form-select" data-live-search="true" required>
                                    <option value="">Campus</option>
                                    @foreach($campus as $row)
                                    <option value="{{$row->id}}">{{$row->campus}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-graduation-cap"></i></span>
                                <select id="facultad" name="facultad" class="form-select" data-live-search="true" required>
                                    <option value="">Facultad</option>
                                    @foreach($facultades as $row)
                                    <option value="{{$row->id}}">{{$row->facultad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-user-tie"></i></span>
                                <select id="tipo" name="tipo" class="form-select" data-live-search="true" required>
                                    <option value="">Tipo de lector</option>
                                    @foreach($tipos as $row)
                                    <option value="{{$row->id}}">{{$row->tipo_lector}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 offset-md-2">
                        <div class="d-grid col-6 mx-auto">
                            <button id="btnSave" class="btn btn-theme" type="subtmit"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/lectores.js') }}"></script>
@endsection