@extends('layouts.app')

@section('page-title')
Reportes
@endsection

@section('content')
    <div class="row" id="app">
        <div class="col-md-12">
            <div class="card border-warning shadow animate__animated animate__zoomInDown">
                <div class="card-header bg-warning"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                @if(session()->get('success'))
                                    <div class="alert alert-success alert-fade">
                                        <i class="fa-solid fa-check"></i>
                                        {{ session()->get("success") }}
                                    </div>
                                  @endif
                                  @if(session()->get('error'))
                                    <div class="alert alert-danger alert-fade">
                                        <i class="fa-solid fa-xmark"></i>
                                        {{ session()->get("error") }}
                                    </div>
                                  @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-file-alt"></i></span>
                                    <select class="form-select" id="reporte" required>
                                        <option value="">Reporte</option>
                                        <option value="1">Inventario de ejemplares</option>
                                        <option value="2">Libros más prestados</option>
                                        <option value="3">Libros que no se han prestado</option>
                                        <option value="4">Ejemplares en préstamo</option>
                                        <option value="5">Lectores que han sobrepado su fecha de entrega</option>
                                        <option value="6">Historial de préstamos</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 ocultar d-none">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-calendar-alt"></i></span>
                                    <input type="date" class="form-control" id="f_ini" name="f_ini" data-bs-toggle="tooltip" title="Fecha inicio">
                                </div>
                            </div>
                            <div class="col-md-4 ocultar d-none">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-calendar-alt"></i></span>
                                    <input type="date" class="form-control" id="f_fin" name="f_fin" data-bs-toggle="tooltip" title="Fecha fin">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button id="btnBuscar" class="btn btn-theme"><i class="fa-solid fa-magnifying-glass"></i> Consultar</button>
                            </div>
                        </div>
                        <hr class="dropdown-divider mt-3 mb-3 d-none">
                        <div class="row resultados d-none" id="reporte_1">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tabla_1" class="table table-bordered table-striped">
                                        <thead><tr><th>#</th><th>ISBN</th><th>TITULO</th><th>CODIGO</th><th>ESTATUS</th><th>PASILLO</th><th>ESTANTE</th></tr></thead>
                                        <tbody id="tbody_1"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row resultados d-none" id="reporte_2">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tabla_2" class="table table-bordered table-striped">
                                        <thead><tr><th>#</th><th>ISBN</th><th>TITULO</th><th>PRESTAMOS</th></tr></thead>
                                        <tbody id="tbody_2"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row resultados d-none" id="reporte_3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tabla_3" class="table table-bordered table-striped">
                                        <thead><tr><th>#</th><th>ISBN</th><th>TITULO</th></tr></thead>
                                        <tbody id="tbody_3"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row resultados d-none" id="reporte_4">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tabla_4" class="table table-bordered table-striped">
                                        <thead><tr><th data-breakpoints="xs">#</th><th>MATRICULA</th><th>LECTOR</th><th>LIBRO</th><th>CODIGO</th><th data-breakpoints="xs sm">IDENTIFICACION</th><th>FECHA PRESTAMO</th><th data-breakpoints="xs">FECHA ENTREGA</th><th data-breakpoints="xs sm">DIAS ATRASO</th></tr></thead>
                                        <tbody id="tbody_4"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row resultados d-none" id="reporte_5">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tabla_5" class="table table-bordered table-striped">
                                        <thead><tr><th data-breakpoints="xs">#</th><th>MATRICULA</th><th>LECTOR</th><th>LIBRO</th><th>FECHA PRESTAMO</th><th data-breakpoints="xs">FECHA ENTREGA</th><th data-breakpoints="xs sm">FECHA ENTREGA REAL</th><th data-breakpoints="xs sm">DIAS ATRASO</th></tr></thead>
                                        <tbody id="tbody_5"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row resultados d-none" id="reporte_6">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tabla_6" class="table table-bordered table-striped">
                                        <thead><tr><th data-breakpoints="xs">#</th><th>MATRICULA</th><th>LECTOR</th><th>LIBRO</th><th>CODIGO</th><th data-breakpoints="xs sm">IDENTIFICACION</th><th>FECHA PRESTAMO</th><th data-breakpoints="xs">FECHA ENTREGA</th><th data-breakpoints="xs sm">FECHA ENTREGA REAL</th><th data-breakpoints="xs sm">DIAS ATRASO</th></tr></thead>
                                        <tbody id="tbody_6"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row d-none" id="load">
                            <div class="col-md-4 offset-md-4">
                                <img src="{{ asset('images/loading_'.rand(1, 12).'.gif')}}" class="img-fluid" height="100%" width="100%"> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/reportes.js') }}"></script>
@endsection